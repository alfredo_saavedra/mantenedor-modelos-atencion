(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .constant("MODELO_ATENCION.TIPOS_ERROR",{
            EXITOSO: "S",
            FALLIDO: "E",
            WARNING: "W",
            INFO: "I"
        })
        .constant("MODELO_ATENCION.EXCEPCIONES",{
            errorNotificacionEjecutivoPool: {
                title: "No se a podido realizar la operación",
                message: "No se a podido realizar la operación",
                tipo: "E"
            },
            exitoModelo: {
                title: "Operación realizada",
                message: "Operación realizada",
                tipo: "S"
            },
            modeloYaExiste: {
                title: "Esta combinación de parámetros ya existe, intente con otra",
                message: "Esta combinación de parámetros ya existe, intente con otra",
                tipo: "E"
            },
            warningModeloYaExiste: {
                title: "Esta combinación de parámetros ya existe, intente con otra",
                message: "Esta combinación de parámetros ya existe, intente con otra",
                tipo: "W"
            },
            operacionNoRealizada: {
                title: "No se a podido realizar la operación",
                message: "No se a podido realizar la operación",
                tipo: "E"
            },
            servicioNoDisponible: {
                title: "Servicio no disponible",
                message: "Servicio no disponible",
                tipo: "E"
            },
            exitoListadoEliminados: {
                title: "Operación realizada",
                message: "Operación realizada",
                tipo: "S"
            },
            sinResultadosMsg: {
                title: "No se encontraron resultados",
                message: "No se encontraron resultados",
                tipo: "I"
            },
            operacionFallida: {
                title: "No se ha podido realizar la operación",
                message: "No se ha podido realizar la operación",
                tipo: "E"
            },
            comentariosNoDisponibles: {
                title: "No existen comentarios",
                message: "No existen comentarios de edición para esta combinación",
                tipo: "I"
            },
            responseOk: {
                title: "Operación realizada",
                message: "Operación realizada",
                tipo: "S"
            },
            errorEliminarVigentes: {
                title: "Falló al eliminar",
                message: "No se ha podido realizar la operación",
                tipo: "E"
            },
            errorRestaurarEliminados: {
                title: "Falló al restaurar",
                message: "No se ha podido realizar la operación",
                tipo: "E"
            }
        })
        .constant("MODELO_ATENCION.VARIABLES", {
            fechas: {
                SEMANA: {
                    nombre: "SEMANA",
                    ctDias: 7
                },
                MES: {
                    nombre: "MES",
                    ctDias: 30
                }
            },
            modelosAtencion:{
                vigentes: {
                    estado: "V",
                    configModalConfirmaEliminacion: {
                        title: "",
                        text: "¿Está seguro que desea eliminar estos elementos?",
                        type: "warning",
                        customClass: "sweet-modales",
                        showCancelButton: true,
                        animation: "slide-from-top",
                        confirmButtonColor: "#002058",
                        confirmButtonText: "Eliminar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }
                },
                eliminados: {
                    estado: "E",
                    configModalConfirmaRestauracion: {
                        title: "",
                        text: "¿Está seguro que desea restaurar estos elementos?",
                        type: "warning",
                        customClass: "sweet-modales",
                        showCancelButton: true,
                        animation: "slide-from-top",
                        confirmButtonColor: "#002058",
                        confirmButtonText: "Restaurar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }
                }
            },
            paginadorVigentes: {
                itemsPerPage: [10, 30, 50, 100],
                pageSize: 10,
                currentPage: 1,
                totalItems: 0,
                emptyMessage: 'No se encontraron resultados',
                maxSize: 3,
                previousText: '‹',
                nextText: '›',
                firstText: '«',
                lastText: '»'
            },
            paginadorNoVigentes: {
                itemsPerPage: [10, 30, 50, 100],
                pageSize: 10,
                currentPage: 1,
                totalItems: 0,
                emptyMessage: 'No se encontraron resultados',
                maxSize: 3,
                previousText: '‹',
                nextText: '›',
                firstText: '«',
                lastText: '»'
            },
            titulos: {
                agregar: "Agregar",
                editar: "Editar"
            },
            minCaracteresComentario: 10,
            limiteCaracteresComentario: 200,
            labelSucursalPool: "POOL",
            segmentoComercialAsterisco: {
                idIdentificador: 0,
                valor: "*"
            }
        })
        .constant("MODELO_ATENCION.TAGS", {
            getParametros: {
                rs: {
                    ok:{
                        codRespuesta: 0,
                        msjRespuesta: "OK"
                    }
                }
            },
            listarValores: {
                rq: {
                    tipo: "FIN_CON_VALUE_TYPE",
                    identificador: "1-19W3J-2175",
                    usuario: ""
                }
            },
            agregarModelos: {
                rs: {
                    ok:{
                        codRespuesta: "0000",
                        msjRespuesta: "OK"
                    },
                    modeloYaExiste:{
                        codRespuesta: "0004",
                        msjRespuesta: "Esta combinacion de parametros ya existe, intente con otra "
                    }
                }
            },
            listarModelos: {
                rs: {
                    ok:{
                        codRespuesta: "0000",
                        msjeRespuesta: "OK"
                    }
                }
            },
            estadoModelos: {
                rs: {
                    ok:{
                        codRespuesta: "0000",
                        msjeRespuesta: "OK"
                    },
                    modeloYaExiste:{
                        codRespuesta: "0004",
                        msjRespuesta: "Esta combinacion de parametros ya existe, intente con otra "
                    }
                }
            },
            editarModelos: {
                rs: {
                    ok:{
                        codRespuesta: "0000",
                        msjRespuesta: "OK"
                    },
                    modeloYaExiste:{
                        codRespuesta: "0004",
                        msjRespuesta: "Esta combinacion de parametros ya existe, intente con otra "
                    }
                }
            }
        })
    ;
})();