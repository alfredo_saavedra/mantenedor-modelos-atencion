(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("modelosAtencionService", modelosAtencionService);
        modelosAtencionService.$inject = ['$q','modelosAtencionFactory'];

    function modelosAtencionService($q, modelosAtencionFactory) {
        var serv = this;
        serv.$q = $q;
        serv.modelosAtencionFactory = modelosAtencionFactory;
    }

    /**
     *  @description método que entrega label a mostrar en vista de las notificaciones
     *  @param {array} notificaciones
     *      @param  {int} idNotificacion
     *      @param  {string} descripcion
     *      @param  {int} idModeloAtencion
     *  @param {array} separador
     *  @returns {string} notificacionesSeparadasPorSeparador
     */
    modelosAtencionService.prototype.getLabelNotificaciones = function(notificaciones, separador) {
        if(!notificaciones || notificaciones.length === 0) {
            return "-";
        }
        var nombresNotificaciones = notificaciones.map(function(notificacion){
            return notificacion.descripcion;
        });
        return nombresNotificaciones.length > 0 ? nombresNotificaciones.join(separador + " ") : "";
    };

    /**
     *  @description método que lista modelos de atención según su tipo (stVigente)
     *  @param {object}
     *  @param {string} marca
     *  @param {string} segComercial
     *  @param {string} tpSucursal
     *  @param {string} codCliente
     *  @param {string} stVigente (V: vigente, E: eliminado)
     *  @param {string} codSegEstructural
     *  @param {string} codCanal
     *  @param {string} codSegComercial
     *  @param {string} inicio
     *  @param {string} fin
     *  @returns {Promise[response]} data
     *      @param  {string} codRespuesta
     *      @param  {string} idModeloAtencion
     *      @param  {string} msjeRespuesta
     *      @param  {int} totalReg
     *      @param  {array} modeloAtencion
     *          @param  {int} idModeloAtencion
     *          @param  {string} stVigente: (V: vigente; E: eliminado)
     *          @param  {string} marca (codigo)
     *          @param  {int} codCanal
     *          @param  {string} canal
     *          @param  {int} cliente
     *          @param  {string} desTpCliente
     *          @param  {int} codSegComercial
     *          @param  {string} segmentoComercial
     *          @param  {int} codSegEstructural
     *          @param  {string} segmentoEstructural
     *          @param  {string} tipoSucursal
     *          @param  {string} desTpSucursal
     *          @param  {long} fchIngreso
     *          @param  {long} fchModificacion
     *          @param  {int} idEjecutivoMod
     *          @param  {string} tipoNotificaciones
     *      @param  {array} tipoNotificaciones
     *          @param  {int} idNotificacion
     *          @param  {string} descripcion
     *          @param  {int} idModeloAtencion
     */
    modelosAtencionService.prototype.obtenerListadoModelos = function (listarModelosRq) {
        var serv = this;
        return serv.modelosAtencionFactory.listarModelos(listarModelosRq);
    };

    modelosAtencionService.prototype.obtenerListadoNoVigentes = function (listarNoVigentesRq) {
        var serv = this;
        return serv.modelosAtencionFactory.listarNoVigentes(listarNoVigentesRq);
    };

    /**
     *  @description método que actualiza estado (E:eliminado, V:vigente) de uno o muchos modelos de atención
     *  @param {object} request
     *           {string} idEjecutivo
     *           {array} listModelos
     *              {string} idModeloAtencion
     *              {string} stVigente
     *  @returns {Promise[response]}.data
     */
    modelosAtencionService.prototype.estadoModelos = function (modelosAtencionRq) {
        var serv = this;
        return serv.modelosAtencionFactory.estadoModelos(modelosAtencionRq);
    };

    /**
     *  @description método que crea un nuevo modelos de atención
     *  @param {object} modeloAtencionRq
     *           {string} canal
     *           {string} codCanal
     *           {string} marca
     *           {string} codSegEstructural
     *           {string} segmentoEstructural
     *           {string} codSegComercial
     *           {string} segmentoComercial
     *           {string} tipoSucursal
     *           {string} desTpSucursal
     *           {string} cliente
     *           {string} desTpCliente
     *           {array} tipoNotificaciones
     *              {string} idNotificacion
     *  @returns {Promise[response]}.data
     */
    modelosAtencionService.prototype.crearNuevo = function (modeloAtencionRq) {
        var serv = this;
        return serv.modelosAtencionFactory.crearNuevo(modeloAtencionRq);
    };

    /**
     *  @description método que edita un modelos de atención
     *  @param {array} modeloAtencion
     *           {string} id
     *           {string} canal
     *           {string} marca
     *           {string} cliente
     *           {string} segmentoEstructural
     *           {string} segmentoComercial
     *           {string} tipoSucursal
     *           {string} notificacion
     *  @returns {Promise[response]}.data
     */
    modelosAtencionService.prototype.editar = function (modeloAtencionRq) {
        var serv = this;
        return serv.modelosAtencionFactory.editar(modeloAtencionRq);
    };

    /**
     *  @description método que obtiene registro historico de ediciones realizadas en un modelo de atención
     *  @param {object} vigenteEliminado
     *           {string} canal
     *           {string} marca
     *           {string} cliente
     *           {string} segmentoEstructural
     *           {string} segmentoComercial
     *           {string} tipoSucursal
     *           {string} ejecutivo
     *           {string} fechaHora
     *  @returns {Promise[response]}.data
     *           {string} name
     *           {string} position
     *           {string} editionDate
     *           {string} changeDone
     */
    modelosAtencionService.prototype.obtenerHistoricoCambios = function (modeloAtencionRq) {
        var serv = this;
        return serv.modelosAtencionFactory.historicoCambios(modeloAtencionRq);
    };

})();