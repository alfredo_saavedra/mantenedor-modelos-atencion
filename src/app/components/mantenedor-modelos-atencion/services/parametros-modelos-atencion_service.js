(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("parametrosModelosAtencionService", parametrosModelosAtencionService);
        parametrosModelosAtencionService.$inject = ['parametrosModelosAtencionFactory'];

    function parametrosModelosAtencionService(parametrosModelosAtencionFactory) {
        var serv = this;
        serv.parametrosModelosAtencionFactory = parametrosModelosAtencionFactory;
    }

    /**
     *  @description método que obtiene parametros utilizados para filtrar/crear/editar los modelos de atención
     *  @returns {Promise[response]}.data
     *           {array} listCliente
     *                  {int} codTpCliente
     *                  {string} desTpCliente
     *           {array} listMarca
     *                  {int} codTpCliente
     *                  {string} desTpCliente
     *           {array} listCanal
     *                  {int} marca
     *                  {string} desMarca
     *           {array} listSegEst
     *                  {int} codSegmentoEst
     *                  {string} desSegmentoEst
     *           {array} listSucursal
     *                  {int} codTpSucursal
     *                  {string} desTpSucursal
     *           {array} listNoti
     *                  {int} idNotificacion
     *                  {string} desNotificacion
     *           {int} codRespuesta
     *           {string} msjRespuesta
     */
    parametrosModelosAtencionService.prototype.getParametros = function() {
        var serv = this;
        return serv.parametrosModelosAtencionFactory.getParametros();
    };

    /**
     *  @description método que obtiene parametros segmentos comerciales
     *  los parametros de los modelos de atención
     *  @param {object} consultaValoresRq
     *           {string} tipo
     *           {string} identificador
     *           {string} usuario
     *  @returns {Promise[response]}.data
     */
    parametrosModelosAtencionService.prototype.consultarValores = function (consultaValoresRq) {
        var serv = this;
        return serv.parametrosModelosAtencionFactory.consultaValores(consultaValoresRq);
    };

    /**
     *  @description método que obtiene listado de ejecutivos que han realizado eliminaciones
     *  @param {object} cargarEjecutivosRq
     *           {string} fechaInicio
     *           {string} fechaFin
     *  @returns {Promise[response]}.data
     *           {array} ejecutivoEliminadoTos
     *              {string} codUsuario
     *              {string} nombre
     *           {string} codigoRespuesta
     *           {string} mensajeRespuesta
     */
    parametrosModelosAtencionService.prototype.cargarEjecutivos = function (cargarEjecutivosRq) {
        var serv = this;
        return serv.parametrosModelosAtencionFactory.cargarEjecutivos(cargarEjecutivosRq);
    };

})();