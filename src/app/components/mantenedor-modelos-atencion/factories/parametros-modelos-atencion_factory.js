(function () {

    angular.module("WebBancoChile.mantenedor2020")
        .factory("parametrosModelosAtencionFactory", parametrosModelosAtencionFactory);
        parametrosModelosAtencionFactory.$inject = ['$http','$filter'];

    function parametrosModelosAtencionFactory($http,$filter) {

        var urlBaseConsulta = $filter('propertiesFilter')('${URL_API.mantenedor-modelo-atencion2020-rest}');

        return {
            getParametros:getParametros,
            consultaValores:consultaValores,
            cargarEjecutivos:cargarEjecutivos
        };

        function getParametros(){
            var urlConsulta = urlBaseConsulta + "/consulta/cargarFiltros";
            var config = {
                method: 'POST',
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function consultaValores(consultaValoresRq){
            var urlConsulta =  urlBaseConsulta +  "/consulta/listarValores";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: consultaValoresRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function cargarEjecutivos(cargarEjecutivosRq){
            var urlConsulta =  urlBaseConsulta +  "/consulta/cargarEjecutivosEliminados";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: cargarEjecutivosRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

    }

})();
