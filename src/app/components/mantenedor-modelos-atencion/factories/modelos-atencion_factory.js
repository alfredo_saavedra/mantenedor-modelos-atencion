(function () {

    angular.module("WebBancoChile.mantenedor2020")
        .factory("modelosAtencionFactory", modelosAtencionFactory);
        modelosAtencionFactory.$inject = ['$http','$filter'];

    function modelosAtencionFactory($http,$filter) {

        var urlBaseConsulta = $filter('propertiesFilter')('${URL_API.mantenedor-modelo-atencion2020-rest}');

        return {
            listarModelos:listarModelos,
            estadoModelos:estadoModelos,
            listarNoVigentes:listarNoVigentes,
            crearNuevo:crearNuevo,
            editar:editar,
            historicoCambios:historicoCambios
        };

        function listarModelos(listarModelosRq){
            var urlConsulta = urlBaseConsulta + "/mantenedor/listarModelos";
            var config = {
                method: 'POST',
                data: listarModelosRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function estadoModelos(modelosAtencionRq){
            var urlConsulta = urlBaseConsulta + "/mantenedor/estadoModelos";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: modelosAtencionRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function listarNoVigentes(listarNoVigentesRq){
            var urlConsulta = urlBaseConsulta + "/mantenedor/listarNoVigentes";
            var config = {
                method: 'POST',
                data: listarNoVigentesRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function crearNuevo(modeloAtencionRq){
            var urlConsulta =  urlBaseConsulta +  "/mantenedor/agregarModelos";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: modeloAtencionRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function editar(modeloAtencionRq){
            var urlConsulta = urlBaseConsulta + "/mantenedor/editarModelos";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: modeloAtencionRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function historicoCambios(vigenteEliminado){
            var urlConsulta =  urlBaseConsulta +  "/consulta/registroCambios/" + vigenteEliminado;
            var config = {
                method: 'GET',
                url: urlConsulta,
                data: vigenteEliminado,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

    }

})();
