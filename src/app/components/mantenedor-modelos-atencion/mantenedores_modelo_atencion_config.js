(function() {
    angular
        .module('WebBancoChile.mantenedor2020')
        .config(mantenedorModeloAtencion2020Config);

    function mantenedorModeloAtencion2020Config($stateProvider, $urlRouterProvider, TABS) {
        $stateProvider.state('mantenedor-modelos-atencion', {
            parent: 'root',
            url: '/mantenedor-modelos-atencion',
            views: {
                "main@": {
                    controller: 'modelosAtencionHomeCtrl as vm',
                    templateUrl: 'components/mantenedor-modelos-atencion/views/modelos-atencion-home_tpl.html'
                }
            }, data: { pageTitle: 'Mantenedor Modelos Atención 2020' }
        })
        .state('atencion-vigentes', {
            parent: 'mantenedor-modelos-atencion',
            url: '/atencion-vigentes/',
            views: {
                "contenido_tab_view": {
                    controller: 'modelosAtencionVigentesCtrl as vm',
                    templateUrl: 'components/mantenedor-modelos-atencion/views/modelos-atencion-vigentes_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.modelosAtencion.vigentes, TABS.modelosAtencion);
                }
            }
        })
        .state('atencion-eliminados', {
            parent: 'mantenedor-modelos-atencion',
            url: '/atencion-eliminados/',
            views: {
                "contenido_tab_view": {
                    controller: 'modelosAtencionEliminadosCtrl as vm',
                    templateUrl: 'components/mantenedor-modelos-atencion/views/modelos-atencion-eliminados_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.modelosAtencion.eliminados, TABS.modelosAtencion);
                }
            }
        });

        $urlRouterProvider.when('/mantenedor-modelos-atencion', '/mantenedor-modelos-atencion/atencion-vigentes/');
    }
})();