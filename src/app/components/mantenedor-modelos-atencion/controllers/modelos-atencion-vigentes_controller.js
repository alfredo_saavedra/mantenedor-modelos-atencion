(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('modelosAtencionVigentesCtrl', modelosAtencionVigentesCtrl);

    function modelosAtencionVigentesCtrl($scope, $modal, $injector, modelosAtencionService,
        parametrosModelosAtencionService, $filter, PerfilUsuarioService){
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.EXCEPCIONES = $injector.get("MODELO_ATENCION.EXCEPCIONES");
        vm.VARIABLES = $injector.get("MODELO_ATENCION.VARIABLES");
        vm.TAGS = $injector.get("MODELO_ATENCION.TAGS");
        vm.modelosAtencionService = modelosAtencionService;
        vm.getLabelNotificaciones = vm.modelosAtencionService.getLabelNotificaciones;
        vm.parametrosModelosAtencionService = parametrosModelosAtencionService;
        vm.$filter = $filter;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.vigentesEliminados = [];
        vm.paginador = vm.VARIABLES.paginadorVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.initMantenedor();
        $scope.$on('actualizaVigentes', function(){
            vm.initMantenedor();
        });
    }

    modelosAtencionVigentesCtrl.prototype.initMantenedor = function() {
        var vm = this;
        vm.obtenerParametros();
        vm.actualizaVigentes();
    };

    modelosAtencionVigentesCtrl.prototype.obtenerParametros = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.parametrosModelosAtencionService.getParametros().then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) && rs.data.codRespuesta === vm.TAGS.getParametros.rs.ok.codRespuesta &&
                rs.data.msjRespuesta === vm.TAGS.getParametros.rs.ok.msjRespuesta) {
                vm.PARAMETROS = rs.data;
                vm.initFiltros();
            }
        });
    };

    modelosAtencionVigentesCtrl.prototype.initFiltros = function () {
        var vm = this;
        vm.filtrosCanales = vm.PARAMETROS.listCanal;
        vm.filtrosMarcas = vm.PARAMETROS.listMarca;
        vm.filtrosClientes = vm.PARAMETROS.listCliente;
        vm.filtrosSegmentosEstructurales = vm.PARAMETROS.listSegEst;
        vm.getFiltroSegmentoComercial();
        vm.filtrosTiposSucursales = vm.PARAMETROS.listSucursal;
    };

    modelosAtencionVigentesCtrl.prototype.getFiltroSegmentoComercial = function () {
        var vm = this;
        var request = vm.TAGS.listarValores.rq;
        request.usuario = vm.perfilUsuario.usuario;
        vm.parametrosModelosAtencionService.consultarValores(request).then(function(rs){
            if(vm.isUndefinedOrNull(rs.errorCode)) {
                vm.filtrosSegmentosComerciales = rs.data;
                vm.filtrosSegmentosComerciales.unshift(vm.VARIABLES.segmentoComercialAsterisco);
            }
        });
    };

    modelosAtencionVigentesCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaVigentes();
    };

    modelosAtencionVigentesCtrl.prototype.reiniciaPagina = function() {
        var vm = this;
        vm.paginador.currentPage = 1;
    };

    modelosAtencionVigentesCtrl.prototype.actualizaVigentes = function() {
        var vm = this;
        var listadoModelosRq = vm.getListarModelosRq();
        vm.vigentesEliminados = [];
        vm.modelosAtencionService.obtenerListadoModelos(listadoModelosRq).then(function(rs){
            vm.servicioNoDisponible = undefined;
            if(vm.isUndefinedOrNull(rs.data)) {
                vm.errorvigentes = vm.EXCEPCIONES.errorvigentes;
                return;
            } else if(rs.data.codRespuesta === vm.TAGS.listarModelos.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.listarModelos.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.modeloAtencion;
                vm.paginador.totalItems = rs.data.totalReg;
            }

            if(rs.data.modeloAtencion.length === 0) {
                vm.sinResultadosMsg = vm.EXCEPCIONES.sinResultadosMsg;
            } else {
                vm.exitovigentes = vm.EXCEPCIONES.exitovigentes;
            }
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    modelosAtencionVigentesCtrl.prototype.getListarModelosRq = function() {
        var vm = this;
        var listadoModelosRq = {};

        listadoModelosRq.marca = vm.isUndefinedOrNull(
            vm.filtroMarca) ? "" : vm.filtroMarca.marca;
        listadoModelosRq.tpSucursal = vm.isUndefinedOrNull(
            vm.filtroTipoSucursal) ? "" : vm.filtroTipoSucursal.codTpSucursal.toString();
        listadoModelosRq.codCliente = vm.isUndefinedOrNull(
            vm.filtroCliente) ? "" : vm.filtroCliente.codTpCliente.toString();
        listadoModelosRq.codSegEstructural = vm.isUndefinedOrNull(
            vm.filtroSegmentoEstructural) ? "" : vm.filtroSegmentoEstructural.codSegmentoEst.toString();
        listadoModelosRq.codCanal = vm.isUndefinedOrNull(
            vm.filtroCanal) ? "" : vm.filtroCanal.codCanal.toString();
        listadoModelosRq.segComercial = vm.isUndefinedOrNull(
            vm.filtroSegmentoComercial) ? "" : vm.filtroSegmentoComercial.valor;
        listadoModelosRq.codSegComercial = vm.isUndefinedOrNull(
            vm.filtroSegmentoComercial) ? "" : vm.filtroSegmentoComercial.idIdentificador.toString();
        listadoModelosRq.stVigente = vm.VARIABLES.modelosAtencion.vigentes.estado;

        listadoModelosRq.pagina = vm.paginador.currentPage.toString();
        listadoModelosRq.elementos = vm.paginador.itemsPerPage.selected.toString();

        return listadoModelosRq;
    };

    modelosAtencionVigentesCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroCanal = undefined;
        vm.filtroMarca = undefined;
        vm.filtroCliente = undefined;
        vm.filtroSegmentoEstructural = undefined;
        vm.filtroSegmentoComercial = undefined;
        vm.filtroTipoSucursal = undefined;
    };

    modelosAtencionVigentesCtrl.prototype.eliminarVigentes = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.modelosAtencion.vigentes.configModalConfirmaEliminacion, function(isConfirm) {
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.vigentesEliminados);
                vm.modelosAtencionService.estadoModelos(cambiarEstadoRq).then(function(rs){
                    if(vm.isUndefinedOrNull(rs.data)) {
                        vm.errorEliminarVigentes = vm.EXCEPCIONES.errorEliminarVigentes;
                    }
                    if(rs.data.codRespuesta === vm.TAGS.estadoModelos.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoModelos.rs.ok.msjeRespuesta) {
                        vm.responseOk = vm.EXCEPCIONES.responseOk;
                    } else {
                        vm.errorEliminarVigentes = vm.EXCEPCIONES.errorEliminarVigentes;
                    }
                    vm.actualizaVigentes();
                    vm.vigentesEliminados = [];
                }, function(){
                    vm.errorEliminarVigentes = vm.EXCEPCIONES.errorEliminarVigentes;
                });
            }
        });
    };

    modelosAtencionVigentesCtrl.prototype.eliminarVigente = function(vigente) {
        var vm = this;
        var cambiarEstadoRq = vm.getCambiarEstadoRq([vigente]);
        vm.limpiarExcepciones();
        vm.errorEliminaVigente = false;
        vm.modelosAtencionService.estadoModelos(cambiarEstadoRq).then(function(rs){
            if(vm.isUndefinedOrNull(rs.data)) {
                vm.errorEliminaVigente = true;
            }
            if(rs.data.codRespuesta === vm.TAGS.estadoModelos.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoModelos.rs.ok.msjeRespuesta) {
                vm.responseOk = vm.EXCEPCIONES.responseOk;
            } else {
                vm.errorEliminaVigente = true;
            }
            vm.actualizaVigentes();
        }, function() {
            vm.errorEliminaVigente = true;
        });
    };

    modelosAtencionVigentesCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};

        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.listModelos = [];
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listModelos.push(
                {
                    idModeloAtencion: eliminado.idModeloAtencion,
                    stVigente: vm.VARIABLES.modelosAtencion.eliminados.estado
                }
            );
        });

        return cambiarEstadoRq;
    };

    modelosAtencionVigentesCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.vigentesEliminados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.vigentesEliminados.push(elemento);
        } else {
            vm.vigentesEliminados.splice(idxListadoSeleccionados, 1);
        }
    };

    modelosAtencionVigentesCtrl.prototype.asideAgregarModeloAtencion = function(size) {
        var vm = this;
        var modalInstance = vm.$modal.open({
            templateUrl: 'components/mantenedor-modelos-atencion/views/aside-mantenedor-modelo-atencion_tpl.html',
            animation: true,
            controller: 'agregarModeloAtencionCtrl as vm',
            size: size,
            windowClass: 'modal-detalle',
            backdrop: 'static'
        });
        modalInstance.perfilUsuario = vm.perfilUsuario;

        modalInstance.result.then(function () {
            vm.actualizaVigentes();
        }, function () {
            vm.actualizaVigentes();
        });
    };

    modelosAtencionVigentesCtrl.prototype.asideEditarModeloAtencion = function(size, vigente) {
        var vm = this;
        var modalInstance = vm.$modal.open({
            templateUrl: 'components/mantenedor-modelos-atencion/views/aside-mantenedor-modelo-atencion_tpl.html',
            animation: true,
            controller: 'editarModeloAtencionCtrl as vm',
            size: size,
            windowClass: 'modal-detalle',
            backdrop: 'static'
        });
        modalInstance.data = vigente;
        modalInstance.perfilUsuario = vm.perfilUsuario;

        modalInstance.result.then(function () {
            vm.actualizaVigentes();
        }, function () {
            vm.actualizaVigentes();
        });
    };

    modelosAtencionVigentesCtrl.prototype.asideRegistroCambios = function (size, vigente) {
        var vm = this;
        var modalInstance = vm.$modal.open({
            templateUrl: 'components/mantenedor-modelos-atencion/views/aside-registro-cambios_tpl.html',
            animation: true,
            controller: 'registroCambiosModeloAtencionCtrl as vm',
            size: size,
            windowClass: 'modal-detalle',
            backdrop: 'static'
        });
        modalInstance.data = vigente;
    };

    modelosAtencionVigentesCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    modelosAtencionVigentesCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.errorvigentes = undefined;
        vm.exitovigentes = undefined;
        vm.errorEliminarVigentes = undefined;
        vm.responseOk = undefined;
        vm.sinResultadosMsg = undefined;
    };

    modelosAtencionVigentesCtrl.prototype.cambiaPagina = function(){
        var vm = this;
        vm.actualizaVigentes();
    };

    modelosAtencionVigentesCtrl.prototype.cambiaCantidadElementos = function(){
        var vm = this;
        vm.paginador.currentPage = 1;
        vm.actualizaVigentes();
    };

})();