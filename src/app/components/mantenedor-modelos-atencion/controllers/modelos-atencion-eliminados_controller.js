(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('modelosAtencionEliminadosCtrl', modelosAtencionEliminadosCtrl);

    function modelosAtencionEliminadosCtrl($scope, $modal, $injector, modelosAtencionService,
        parametrosModelosAtencionService, $filter, fechaUtilService, PerfilUsuarioService){
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.fechaUtilService = fechaUtilService;
        vm.EXCEPCIONES = $injector.get("MODELO_ATENCION.EXCEPCIONES");
        vm.VARIABLES = $injector.get("MODELO_ATENCION.VARIABLES");
        vm.TAGS = $injector.get("MODELO_ATENCION.TAGS");
        vm.modelosAtencionService = modelosAtencionService;
        vm.parametrosModelosAtencionService = parametrosModelosAtencionService;
        vm.$filter = $filter;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.eliminadosRestaurados = [];
        vm.paginador = vm.VARIABLES.paginadorNoVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.initMantenedor();
        $scope.$on('actualizaEliminados', function(){
            vm.initMantenedor();
        });
    }

    modelosAtencionEliminadosCtrl.prototype.initMantenedor = function() {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            vm.fechaDesde = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaHasta = data;
            vm.fechaDesdeMax = data;
            vm.fechaHastaMin = data;
            vm.obtenerParametros();
            vm.actualizaEliminados();
        });
    };

    modelosAtencionEliminadosCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaEliminados();
    };

    modelosAtencionEliminadosCtrl.prototype.reiniciaPagina = function() {
        var vm = this;
        vm.paginador.currentPage = 1;
    };

    modelosAtencionEliminadosCtrl.prototype.obtenerParametros = function() {
        var vm = this;
        vm.parametrosModelosAtencionService.getParametros().then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) && rs.data.codRespuesta === vm.TAGS.getParametros.rs.ok.codRespuesta &&
                rs.data.msjRespuesta === vm.TAGS.getParametros.rs.ok.msjRespuesta) {
                vm.PARAMETROS = rs.data;
                vm.initFiltros();
            }
        });
    };

    modelosAtencionEliminadosCtrl.prototype.initFiltros = function () {
        var vm = this;
        vm.limpiarExcepciones();

        vm.getFiltroEjecutivos();
        vm.filtrosCanales = vm.PARAMETROS.listCanal;
        vm.filtrosMarcas = vm.PARAMETROS.listMarca;
        vm.filtrosClientes = vm.PARAMETROS.listCliente;
        vm.filtrosSegmentosEstructurales = vm.PARAMETROS.listSegEst;
        vm.getFiltroSegmentoComercial();
        vm.filtrosTiposSucursales = vm.PARAMETROS.listSucursal;
    };

    modelosAtencionEliminadosCtrl.prototype.getFiltroEjecutivos = function () {
        var vm = this;
        var cargarEjecutivosRq = vm.getFiltroEjecutivosRq();
        vm.parametrosModelosAtencionService.cargarEjecutivos(cargarEjecutivosRq).then(function(rs){
            if(vm.isUndefinedOrNull(rs.errorCode)) {
                vm.filtrosEjecutivos = rs.data.ejecutivoEliminadoTos;
            }
        });
    };

    modelosAtencionEliminadosCtrl.prototype.getFiltroEjecutivosRq = function() {
        var vm = this;
        var filtroEjecutivos = {};

        filtroEjecutivos.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        filtroEjecutivos.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return filtroEjecutivos;
    };

    modelosAtencionEliminadosCtrl.prototype.getFiltroSegmentoComercial = function () {
        var vm = this;
        var request = vm.TAGS.listarValores.rq;
        request.usuario = vm.perfilUsuario.usuario;
        vm.parametrosModelosAtencionService.consultarValores(request).then(function(rs){
            if(vm.isUndefinedOrNull(rs.errorCode)) {
                vm.filtrosSegmentosComerciales = rs.data;
                vm.filtrosSegmentosComerciales.unshift(vm.VARIABLES.segmentoComercialAsterisco);
            }
        });
    };

    modelosAtencionEliminadosCtrl.prototype.actualizaEliminados = function() {
        var vm = this;
        var listadoModelosRq = vm.getEliminadosRq();
        vm.eliminadosRestaurados = [];
        vm.modelosAtencionService.obtenerListadoNoVigentes(listadoModelosRq).then(function(rs){
            if(vm.isUndefinedOrNull(rs.data)) {
                vm.errorListadoEliminados = vm.EXCEPCIONES.errorListadoEliminados;
            } else if(rs.data.codRespuesta === vm.TAGS.listarModelos.rs.ok.codRespuesta &&
               rs.data.msjeRespuesta === vm.TAGS.listarModelos.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.modeloAtencion;
                vm.paginador.totalItems = rs.data.totalReg;
                vm.exitoListadoEliminados = vm.EXCEPCIONES.exitoListadoEliminados;
            }

            if(rs.data.modeloAtencion.length === 0) {
                vm.sinResultadosMsg = vm.EXCEPCIONES.sinResultadosMsg;
            } else {
                vm.exitoListadoEliminados = vm.EXCEPCIONES.exitoListadoEliminados;
            }
            vm.servicioNoDisponible = undefined;
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    modelosAtencionEliminadosCtrl.prototype.getEliminadosRq = function() {
        var vm = this;
        var listadoModelosRq = {};

        listadoModelosRq.marca = vm.isUndefinedOrNull(
            vm.filtroMarca) ? "" : vm.filtroMarca.marca;
        listadoModelosRq.tpSucursal = vm.isUndefinedOrNull(
            vm.filtroTipoSucursal) ? "" : vm.filtroTipoSucursal.codTpSucursal.toString();
        listadoModelosRq.codCliente = vm.isUndefinedOrNull(
            vm.filtroCliente) ? "" : vm.filtroCliente.codTpCliente.toString();
        listadoModelosRq.codSegEstructural = vm.isUndefinedOrNull(
            vm.filtroSegmentoEstructural) ? "" : vm.filtroSegmentoEstructural.codSegmentoEst.toString();
        listadoModelosRq.codCanal = vm.isUndefinedOrNull(
            vm.filtroCanal) ? "" : vm.filtroCanal.codCanal.toString();
        listadoModelosRq.segComercial = vm.isUndefinedOrNull(
            vm.filtroSegmentoComercial) ? "" : vm.filtroSegmentoComercial.valor;
        listadoModelosRq.codSegComercial = vm.isUndefinedOrNull(
            vm.filtroSegmentoComercial) ? "" : vm.filtroSegmentoComercial.idIdentificador.toString();
        listadoModelosRq.stVigente = vm.VARIABLES.modelosAtencion.eliminados.estado;
        listadoModelosRq.idEjecutivoMod = vm.isUndefinedOrNull(
            vm.filtroEjecutivo) ? "" : vm.filtroEjecutivo.codUsuario;

        listadoModelosRq.pagina = vm.paginador.currentPage.toString();
        listadoModelosRq.elementos = vm.paginador.pageSize.toString();

        listadoModelosRq.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        listadoModelosRq.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return listadoModelosRq;
    };

    modelosAtencionEliminadosCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroCanal = undefined;
        vm.filtroMarca = undefined;
        vm.filtroCliente = undefined;
        vm.filtroSegmentoEstructural = undefined;
        vm.filtroSegmentoComercial = undefined;
        vm.filtroTipoSucursal = undefined;
        vm.filtroEjecutivo = undefined;
    };

    modelosAtencionEliminadosCtrl.prototype.filtrarPorPeriodo = function(periodo) {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            var fechaAhora = data;
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            switch(periodo){
                case vm.VARIABLES.fechas.MES.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.MES.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                case vm.VARIABLES.fechas.SEMANA.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.SEMANA.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                default:
                    break;
            }
            vm.actualizaRangoFechas();
            vm.actualizaEliminados();
        });
    };

    modelosAtencionEliminadosCtrl.prototype.actualizaRangoFechas = function() {
        var vm = this;
        if(vm.fechaDesde > vm.fechaHasta) {
            vm.fechaHasta = vm.fechaDesde;
        }
        vm.limpiarExcepciones();
        vm.reiniciaPagina();
        vm.getFiltroEjecutivos();
    };

    modelosAtencionEliminadosCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    modelosAtencionEliminadosCtrl.prototype.restaurarEliminados = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.modelosAtencion.eliminados.configModalConfirmaRestauracion, function(isConfirm){
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.eliminadosRestaurados);
                vm.modelosAtencionService.estadoModelos(cambiarEstadoRq).then(function(rs){
                    if(vm.isUndefinedOrNull(rs.data)) {
                        vm.errorRestaurarEliminados = vm.EXCEPCIONES.errorEliminarVigentes;
                    } else if(rs.data.codRespuesta === vm.TAGS.estadoModelos.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoModelos.rs.ok.msjeRespuesta) {
                        vm.responseOk = vm.EXCEPCIONES.responseOk;
                    }
                    vm.actualizaEliminados();
                    vm.eliminadosRestaurados = [];
                }, function(){
                    vm.errorRestaurarEliminados = vm.EXCEPCIONES.errorEliminarVigentes;
                });
            }
        });
    };

    modelosAtencionEliminadosCtrl.prototype.restaurarEliminado = function(eleminado) {
        var vm = this;
        vm.limpiarExcepciones();
        var cambiarEstadoRq = vm.getCambiarEstadoRq([eleminado]);
        vm.modelosAtencionService.estadoModelos(cambiarEstadoRq).then(function(rs){
            if(vm.isUndefinedOrNull(rs.data) || (!vm.isUndefinedOrNull(rs.data) &&
               rs.data.codRespuesta !== vm.TAGS.estadoModelos.rs.ok.codRespuesta &&
               rs.data.msjeRespuesta !== vm.TAGS.estadoModelos.rs.ok.msjeRespuesta)) {
                vm.errorRestaurarEliminado = true;
            } else if (rs.data.codRespuesta === vm.TAGS.estadoModelos.rs.modeloYaExiste.codRespuesta) {
                vm.modeloYaExiste = vm.EXCEPCIONES.modeloYaExiste;
            } else if(rs.data.codRespuesta === vm.TAGS.estadoModelos.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoModelos.rs.ok.msjeRespuesta) {
                vm.responseOk = vm.EXCEPCIONES.responseOk;
            }
            vm.actualizaEliminados();
        }, function() {
            vm.errorRestaurarEliminado = true;
        });
    };

    modelosAtencionEliminadosCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};

        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.listModelos = [];
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listModelos.push(
                {
                    idModeloAtencion: eliminado.idModeloAtencion,
                    stVigente: vm.VARIABLES.modelosAtencion.vigentes.estado
                }
            );
        });

        return cambiarEstadoRq;
    };

    modelosAtencionEliminadosCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.eliminadosRestaurados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.eliminadosRestaurados.push(elemento);
        } else {
            vm.eliminadosRestaurados.splice(idxListadoSeleccionados, 1);
        }
    };

    modelosAtencionEliminadosCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.errorListadoEliminados = undefined;
        vm.exitoListadoEliminados = undefined;
        vm.modeloYaExiste = undefined;
        vm.responseOk = undefined;
        vm.errorRestaurarEliminados = undefined;
        vm.sinResultadosMsg = undefined;
    };

})();