(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('modelosAtencionHomeCtrl', modelosAtencionHomeCtrl);

    modelosAtencionHomeCtrl.$inject = ['$scope', '$state', 'TABS'];

    function modelosAtencionHomeCtrl($scope, $state, TABS){
        var vm = this;
        vm.$scope = $scope;
        $scope.$on('servicioNoDisponible', function(evt, message){
            vm.servicioNoDisponible = message;
        });
        vm.TABS = TABS.modelosAtencion;
        vm.$state = $state;
    }

    modelosAtencionHomeCtrl.prototype.reload = function() {
        var vm = this;
        switch(vm.$state.current.name) {
            case vm.TABS.vigentes.route:
                vm.$scope.$broadcast('actualizaVigentes');
                break;
            case vm.TABS.eliminados.route:
                vm.$scope.$broadcast('actualizaEliminados');
                break;
            default:
                break;
        }
    };

})();