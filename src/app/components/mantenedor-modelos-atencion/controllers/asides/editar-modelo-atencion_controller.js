(function(){
    angular.module( 'WebBancoChile.mantenedor2020')
        .controller('editarModeloAtencionCtrl', editarModeloAtencionCtrl);

    function editarModeloAtencionCtrl($scope, $modalInstance, $injector, modelosAtencionService,
        parametrosModelosAtencionService){
        var vm = this;
        vm.$scope = $scope;
        vm.$modalInstance = $modalInstance;
        vm.perfilUsuario = $modalInstance.perfilUsuario;
        vm.modeloAtencionEditar = vm.$modalInstance.data;
        vm.modelosAtencionService = modelosAtencionService;
        vm.parametrosModelosAtencionService = parametrosModelosAtencionService;
        vm.EXCEPCIONES = $injector.get("MODELO_ATENCION.EXCEPCIONES");
        vm.VARIABLES = $injector.get("MODELO_ATENCION.VARIABLES");
        vm.TAGS = $injector.get("MODELO_ATENCION.TAGS");
        vm.accionTituloMantenedor = vm.VARIABLES.titulos.editar;
        vm.editaFlag = true;
        vm.modeloSinEdicion = false;
        vm.notificaciones = [];
        vm.validarClickFono = false;
        vm.obtenerParametros();
        vm.clickFonoSeleccionado();
   
    }

    editarModeloAtencionCtrl.prototype.obtenerParametros = function() {
        var vm = this;
        vm.parametrosModelosAtencionService.getParametros().then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) && rs.data.codRespuesta === vm.TAGS.getParametros.rs.ok.codRespuesta &&
                rs.data.msjRespuesta === vm.TAGS.getParametros.rs.ok.msjRespuesta) {
                vm.PARAMETROS = rs.data;
                vm.getSegmentosComerciales();
            }
        });
    };

    editarModeloAtencionCtrl.prototype.getSegmentosComerciales = function () {
        var vm = this;
        var request = vm.TAGS.listarValores.rq;
        request.usuario = vm.perfilUsuario.usuario;
        vm.parametrosModelosAtencionService.consultarValores(request).then(function(rs){
            if(vm.isUndefinedOrNull(rs.errorCode)) {
                vm.PARAMETROS.segmentosComerciales = rs.data;
                vm.PARAMETROS.segmentosComerciales.unshift(vm.VARIABLES.segmentoComercialAsterisco);
                vm.initEdicion();
            }
        });
    };

    editarModeloAtencionCtrl.prototype.initEdicion = function() {
        var vm = this;

        vm.canal = vm.PARAMETROS.listCanal.find(function(it){
            return (vm.modeloAtencionEditar.canal.toLowerCase() === it.desCanal.toLowerCase());
        });
        vm.marca = vm.PARAMETROS.listMarca.find(function(it){
            return (vm.modeloAtencionEditar.desMarca.toLowerCase() === it.desMarca.toLowerCase());
        });
        vm.cliente = vm.PARAMETROS.listCliente.find(function(it){
            return (vm.modeloAtencionEditar.desTpCliente.toLowerCase() === it.desTpCliente.toLowerCase());
        });
        vm.segmentoEstructural = vm.PARAMETROS.listSegEst.find(function(it){
            return (vm.modeloAtencionEditar.segmentoEstructural.toLowerCase() === it.desSegmentoEst.toLowerCase());
        });
        vm.segmentoComercial = vm.PARAMETROS.segmentosComerciales.find(function(it){
            return (vm.modeloAtencionEditar.segmentoComercial.toLowerCase() === it.valor.toLowerCase());
        });
        vm.tipoSucursal = vm.PARAMETROS.listSucursal.find(function(it){
            return (vm.modeloAtencionEditar.desTpSucursal.toLowerCase() === it.desTpSucursal.toLowerCase());
        });

        vm.minComentarios = vm.VARIABLES.minCaracteresComentario;
        vm.limiteComentarios = vm.VARIABLES.limiteCaracteresComentario;
        vm.comentarios = "";

        if(vm.modeloAtencionEditar.tipoNotificaciones.length > 0){
            vm.notificaciones = {};
            vm.modeloAtencionEditar.tipoNotificaciones.forEach(function(notificacionActual){
                vm.notificaciones[notificacionActual.idNotificacion] =
                    vm.PARAMETROS.listNoti.find(function(notificacionOpcion){
                        return (notificacionActual.idNotificacion === notificacionOpcion.idNotificacion);
                    }) ? true : false;
            });
        }
        vm.preRequest = vm.getRqEditarModelo();
    };

    editarModeloAtencionCtrl.prototype.getEstadoAccion = function() {
        var vm = this;
        return (vm.exitoModelo||vm.operacionNoRealizada||vm.servicioNoDisponible);
    };

    editarModeloAtencionCtrl.prototype.editarModeloAtencion = function() {
        var vm = this;
        if(vm.validacionNotiPool) {
            vm.errorNotificacionEjecutivoPool = vm.EXCEPCIONES.errorNotificacionEjecutivoPool;
            return;
        }

        vm.limpiarExcepciones();
        if (('' + vm.comentarios).length < vm.VARIABLES.limiteCaracteresComentario &&
            vm.$scope.modeloAtencionForm.$valid) {
            var modeloAtencionEditadoRq = vm.getRqEditarModelo();
            vm.modelosAtencionService.editar(modeloAtencionEditadoRq).then(function(rs){
                if(vm.isUndefinedOrNull(rs.data)) {
                    vm.servicioNoDisponible = vm.EXCEPCIONES.servicioNoDisponible;
                } else if (rs.data.codRespuesta === vm.TAGS.editarModelos.rs.modeloYaExiste.codRespuesta) {
                    vm.warningModeloYaExiste = vm.EXCEPCIONES.warningModeloYaExiste;
                } else if (rs.data.codRespuesta === vm.TAGS.editarModelos.rs.ok.codRespuesta) {
                    vm.exitoModelo = vm.EXCEPCIONES.exitoModelo;
                }
            }, function(){
                vm.servicioNoDisponible = vm.EXCEPCIONES.servicioNoDisponible;
            });
        }
    };

    editarModeloAtencionCtrl.prototype.revisaModeloEditado = function() {
        var vm = this;
        var modeloActual = vm.preRequest;
        var modeloEditado = vm.getRqEditarModelo();
        modeloEditado.observacionEd = "";
        vm.modeloSinEdicion = JSON.stringify(modeloEditado) !== JSON.stringify(modeloActual);
    };

    editarModeloAtencionCtrl.prototype.getRqEditarModelo = function() {
        var vm = this;
        var editarModeloAtencionRq = {};

        editarModeloAtencionRq.idModeloAtencionEd = vm.$modalInstance.data.idModeloAtencion;
        editarModeloAtencionRq.canalEd = vm.canal.desCanal;
        editarModeloAtencionRq.codCanalEd = vm.canal.codCanal;
        editarModeloAtencionRq.marcaEd = vm.marca.marca.toString();
        editarModeloAtencionRq.codSegEstructuralEd = vm.segmentoEstructural.codSegmentoEst;
        editarModeloAtencionRq.segmentoEstructuralEd = vm.segmentoEstructural.desSegmentoEst.toString();
        editarModeloAtencionRq.codSegComercialEd = vm.segmentoComercial.idIdentificador.toString();
        editarModeloAtencionRq.segmentoComercialEd = vm.segmentoComercial.valor.toString();
        editarModeloAtencionRq.tipoSucursalEd = vm.tipoSucursal.codTpSucursal.toString();
        editarModeloAtencionRq.clienteEd = vm.cliente.codTpCliente;
        editarModeloAtencionRq.idEjecutivoModEd = vm.perfilUsuario.usuario;
        editarModeloAtencionRq.observacionEd = vm.comentarios;
        editarModeloAtencionRq.nombreEjecuEd = vm.perfilUsuario.nombre;
        editarModeloAtencionRq.cargoEjecuEd = vm.perfilUsuario.rol;
        editarModeloAtencionRq.rutEjecutivoEd = vm.perfilUsuario.rut;
        editarModeloAtencionRq.tipoNotificacionList = vm.getNotificaciones();

        return editarModeloAtencionRq;
    };

    editarModeloAtencionCtrl.prototype.getNotificaciones = function() {
        var vm = this;
        var tipoNotificaciones = [];
        if(!vm.isUndefinedOrNull(vm.notificaciones)) {
            Object.keys(vm.notificaciones).forEach(function(key) {
                if(vm.notificaciones[key]) {
                    tipoNotificaciones.push({idNotificacion: key.toString()});
                }
            });
        }
        return tipoNotificaciones;
    };

    editarModeloAtencionCtrl.prototype.validaNotificacionPool = function() {
        var vm = this;
        if(vm.isUndefinedOrNull(vm.tipoSucursal)) {
            return;
        }

        var notificacionPool = vm.PARAMETROS.listNoti.find(function(notificacion){
            return (notificacion.desNotificacion === vm.VARIABLES.labelSucursalPool);
        });

        var poolEstaSeleccionada = Object.keys(vm.notificaciones).find(function(key){
            return (notificacionPool.idNotificacion.toString() === key.toString());
        });

        vm.validacionNotiPool = false;
        //selecciona sucursal no pool y activa notificacion pool
        if(vm.tipoSucursal.desTpSucursal !== notificacionPool.desNotificacion &&
           vm.notificaciones[poolEstaSeleccionada]) {
            vm.validacionNotiPool = true;
        }
    };

    editarModeloAtencionCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.exitoModelo = undefined;
        vm.operacionNoRealizada = undefined;
        vm.servicioNoDisponible = undefined;
        vm.servicioNoDisponible = undefined;
        vm.notificacionEjecutivoPool = undefined;
        vm.modeloYaExiste = undefined;
    };

    editarModeloAtencionCtrl.prototype.volverAtras = function() {
        var vm = this;
        vm.limpiarExcepciones();
    };

    editarModeloAtencionCtrl.prototype.reintentar = function() {
        var vm = this;
        vm.editarModeloAtencion();
    };

    editarModeloAtencionCtrl.prototype.cerrarAside = function() {
        var vm = this;
        vm.$modalInstance.close();
    };

    editarModeloAtencionCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };
    editarModeloAtencionCtrl.prototype.clickFonoSeleccionado = function () {
        var vm = this;
        var listNotificacionesaEditar = vm.modeloAtencionEditar.tipoNotificaciones;
        function existeIdClickFono(id) {
            return listNotificacionesaEditar.some(function(el) {
              if (el.idNotificacion === id){
                vm.validarClickFono = true;
            } else {
                vm.validarClickFono = false;
            }
            }); 
          }
        existeIdClickFono(1);
    };
    editarModeloAtencionCtrl.prototype.validaNotificacionClickfono = function (notificacion) {
        var vm = this;
        var numeroIdClickfono = notificacion.idNotificacion.toString();
        Object.entries(vm.notificaciones).find(function(key){
           if(key.indexOf(numeroIdClickfono) != -1 && key.indexOf(true) != -1){
                return  vm.validarClickFono = true;
            }else{
               vm.validarClickFono = false;
            }
        });
    };
})();