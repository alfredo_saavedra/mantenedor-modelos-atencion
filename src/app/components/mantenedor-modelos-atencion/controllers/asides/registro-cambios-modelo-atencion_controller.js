(function(){
    angular.module( 'WebBancoChile.mantenedor2020')
        .controller('registroCambiosModeloAtencionCtrl', registroCambiosModeloAtencionCtrl);

    function registroCambiosModeloAtencionCtrl($modalInstance, $injector, modelosAtencionService, $scope){
        var vm = this;
        vm.$modalInstance = $modalInstance;
        vm.$injector = $injector;
        vm.modelosAtencionService = modelosAtencionService;
        vm.$scope = $scope;
        vm.EXCEPCIONES = $injector.get("MODELO_ATENCION.EXCEPCIONES");
        vm.initMantenedor();
    }

    registroCambiosModeloAtencionCtrl.prototype.initMantenedor = function() {
        var vm = this;
        var modeloAtencionVigente = vm.$modalInstance.data;
        vm.modelosAtencionService.obtenerHistoricoCambios(modeloAtencionVigente).then(function(rs){
            vm.handlerExcepcionesObtenerHistoricoCambios(rs);
        }, function(){
            vm.servicioNoDisponible = vm.EXCEPCIONES.servicioNoDisponible;
        });
    };

    registroCambiosModeloAtencionCtrl.prototype.handlerExcepcionesObtenerHistoricoCambios = function(rs) {
        var vm = this;
        if(!rs.data) {
            vm.excepcionRegistroCambios = vm.EXCEPCIONES.servicioNoDisponible;
        } else {
            vm.exitoModelo = vm.EXCEPCIONES.exitoModelo;
            vm.registroDeCambios = rs.data.comentarioToList;
            if (vm.registroDeCambios.length === 0){
                vm.excepcionRegistroCambios = vm.EXCEPCIONES.comentariosNoDisponibles;
            }
        }
    };

    registroCambiosModeloAtencionCtrl.prototype.cerrarAside = function() {
        var vm = this;
        vm.$modalInstance.close();
    };
})();