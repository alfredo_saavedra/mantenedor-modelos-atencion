(function(){
    angular.module( 'WebBancoChile.mantenedor2020')
        .controller('agregarModeloAtencionCtrl', agregarModeloAtencionCtrl);

    function agregarModeloAtencionCtrl($modalInstance, $injector, modelosAtencionService, $scope,
        parametrosModelosAtencionService){
        var vm = this;
        vm.$modalInstance = $modalInstance;
        vm.perfilUsuario = $modalInstance.perfilUsuario;
        vm.$injector = $injector;
        vm.modelosAtencionService = modelosAtencionService;
        vm.$scope = $scope;
        vm.EXCEPCIONES = $injector.get("MODELO_ATENCION.EXCEPCIONES");
        vm.VARIABLES = $injector.get("MODELO_ATENCION.VARIABLES");
        vm.TAGS = $injector.get("MODELO_ATENCION.TAGS");
        vm.accionTituloMantenedor = vm.VARIABLES.titulos.agregar;
        vm.parametrosModelosAtencionService = parametrosModelosAtencionService;
        vm.agregaFlag = true;
        vm.validacionNotiPool = false;
        vm.notificaciones = [];
        vm.validarClickFono = false;
        vm.obtenerParametros();
    }

    agregarModeloAtencionCtrl.prototype.validaNotificacionPool = function() {
        var vm = this;
        if(vm.isUndefinedOrNull(vm.tipoSucursal)) {
            return;
        }

        var notificacionPool = vm.PARAMETROS.listNoti.find(function(notificacion){
            return (notificacion.desNotificacion === vm.VARIABLES.labelSucursalPool);
        });

        var poolEstaSeleccionada = Object.keys(vm.notificaciones).find(function(key){
            return (notificacionPool.idNotificacion.toString() === key.toString());
        });

        vm.validacionNotiPool = false;
        //selecciona sucursal no pool y activa notificacion pool
        if(vm.tipoSucursal.desTpSucursal !== notificacionPool.desNotificacion &&
           vm.notificaciones[poolEstaSeleccionada]) {
            vm.validacionNotiPool = true;
        }
    };

    agregarModeloAtencionCtrl.prototype.obtenerParametros = function() {
        var vm = this;
        vm.parametrosModelosAtencionService.getParametros().then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) && rs.data.codRespuesta === vm.TAGS.getParametros.rs.ok.codRespuesta &&
                rs.data.msjRespuesta === vm.TAGS.getParametros.rs.ok.msjRespuesta) {
                vm.PARAMETROS = rs.data;
            }
        });
        vm.getSegmentosComerciales();
    };

    agregarModeloAtencionCtrl.prototype.getSegmentosComerciales = function () {
        var vm = this;
        var request = vm.TAGS.listarValores.rq;
        request.usuario = vm.perfilUsuario.usuario;
        vm.parametrosModelosAtencionService.consultarValores(request).then(function(rs){
            if(vm.isUndefinedOrNull(rs.errorCode)) {
                vm.PARAMETROS.segmentosComerciales = rs.data;
                vm.PARAMETROS.segmentosComerciales.unshift(vm.VARIABLES.segmentoComercialAsterisco);
            }
        });
    };

    agregarModeloAtencionCtrl.prototype.actualizarArrayNotificaciones = function(notificacion) {
        var vm = this;
        if(!vm.notificaciones[notificacion.idNotificacion]){
            delete vm.notificaciones[notificacion.idNotificacion];
        }
    };
    agregarModeloAtencionCtrl.prototype.validaNotificacionClickfono = function (notificacion) {
        var vm = this;
        var numeroIdClickfono = notificacion.idNotificacion;
        //Se chequea si el id de notificacion clickfono está en el array de notificaciones 
        if(numeroIdClickfono in vm.notificaciones){  
            vm.validarClickFono = true;
        } else{
            vm.validarClickFono = false;
        }
    };


    agregarModeloAtencionCtrl.prototype.crearModeloAtencion = function() {
        var vm = this;
        if(vm.$scope.modeloAtencionForm.$valid) {
            vm.crearNuevo();
        }
    };

    agregarModeloAtencionCtrl.prototype.crearNuevo = function() {
        var vm = this;
        if(vm.validacionNotiPool) {
            vm.errorNotificacionEjecutivoPool = vm.EXCEPCIONES.errorNotificacionEjecutivoPool;
            return;
        }

        vm.limpiarExcepciones();
        var nuevoModeloAtencionRq = vm.getRqAgregarModelo();
        vm.modelosAtencionService.crearNuevo(nuevoModeloAtencionRq).then(function(rs){
            if(vm.isUndefinedOrNull(rs.data)) {
                vm.servicioNoDisponible = vm.EXCEPCIONES.servicioNoDisponible;
            } else if (rs.data.codRespuesta === vm.TAGS.agregarModelos.rs.modeloYaExiste.codRespuesta) {
                vm.modeloYaExiste = vm.EXCEPCIONES.warningModeloYaExiste;
            } else if (rs.data.codRespuesta === vm.TAGS.agregarModelos.rs.ok.codRespuesta) {
                vm.exitoModelo = vm.EXCEPCIONES.exitoModelo;
            }
        }, function(){
            vm.servicioNoDisponible = vm.EXCEPCIONES.servicioNoDisponible;
        });
    };

    agregarModeloAtencionCtrl.prototype.getRqAgregarModelo = function() {
        var vm = this;
        var nuevoModeloAtencionRq = {};

        nuevoModeloAtencionRq.canal = vm.canal.desCanal;
        nuevoModeloAtencionRq.codCanal = vm.canal.codCanal.toString();
        nuevoModeloAtencionRq.marca = vm.marca.marca.toString();
        nuevoModeloAtencionRq.codSegEstructural = vm.segmentoEstructural.codSegmentoEst.toString();
        nuevoModeloAtencionRq.segmentoEstructural = vm.segmentoEstructural.desSegmentoEst;
        nuevoModeloAtencionRq.codSegComercial = vm.segmentoComercial.idIdentificador.toString();
        nuevoModeloAtencionRq.segmentoComercial = vm.segmentoComercial.valor;
        nuevoModeloAtencionRq.tipoSucursal = vm.tipoSucursal.codTpSucursal.toString();
        nuevoModeloAtencionRq.desTpSucursal = vm.tipoSucursal.desTpSucursal;
        nuevoModeloAtencionRq.cliente = vm.cliente.codTpCliente.toString();
        nuevoModeloAtencionRq.desTpCliente = vm.cliente.desTpCliente;
        nuevoModeloAtencionRq.tipoNotificaciones = vm.getNotificaciones();

        return nuevoModeloAtencionRq;
    };

    agregarModeloAtencionCtrl.prototype.getNotificaciones = function() {
        var vm = this;
        var tipoNotificaciones = [];
        if(!vm.isUndefinedOrNull(vm.notificaciones)) {
            Object.keys(vm.notificaciones).forEach(function(key) {
                tipoNotificaciones.push({idNotificacion: key.toString()});
            });
        }
        return tipoNotificaciones;
    };

    agregarModeloAtencionCtrl.prototype.getEstadoAccion = function() {
        var vm = this;
        return (vm.exitoModelo||vm.operacionNoRealizada||vm.servicioNoDisponible||vm.modeloYaExiste);
    };

    agregarModeloAtencionCtrl.prototype.volverAtras = function() {
        var vm = this;
        vm.limpiarExcepciones();
    };

    agregarModeloAtencionCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.exitoModelo = undefined;
        vm.operacionNoRealizada = undefined;
        vm.servicioNoDisponible = undefined;
        vm.servicioNoDisponible = undefined;
        vm.notificacionEjecutivoPool = undefined;
        vm.modeloYaExiste = undefined;
    };

    agregarModeloAtencionCtrl.prototype.reintentar = function() {
        var vm = this;
        vm.crearModeloAtencion();
    };

    agregarModeloAtencionCtrl.prototype.cerrarAside = function() {
        var vm = this;
        vm.$modalInstance.close();
    };

    agregarModeloAtencionCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

})();