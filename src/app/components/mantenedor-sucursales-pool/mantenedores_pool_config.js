(function() {
    angular
        .module('WebBancoChile.mantenedor2020')
        .config(mantenedorPool2020Config);

    function mantenedorPool2020Config($stateProvider, $urlRouterProvider, TABS) {
        $stateProvider.state('mantenedor-sucursales-pool', {
            parent: 'root',
            url: '/mantenedor-sucursales-pool',
            views: {
                "main@": {
                    controller: 'sucursalesPoolHomeCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-pool/views/sucursales-pool-home_tpl.html'
                }
            }, data: { pageTitle: 'Mantenedor Sucursales Pool 2020' }
        })
        .state('pool-vigentes', {
            parent: 'mantenedor-sucursales-pool',
            url: '/pool-vigentes/',
            views: {
                "contenido_tab_view": {
                    controller: 'sucursalesPoolVigentesCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-pool/views/sucursales-pool-vigentes_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.sucursalesPool.vigentes, TABS.sucursalesPool);
                }
            }
        })
        .state('pool-eliminados', {
            parent: 'mantenedor-sucursales-pool',
            url: '/pool-eliminados/',
            views: {
                "contenido_tab_view": {
                    controller: 'sucursalesPoolEliminadosCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-pool/views/sucursales-pool-eliminadas_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.sucursalesPool.eliminados, TABS.sucursalesPool);
                }
            }
        });

        $urlRouterProvider.when('/mantenedor-sucursales-pool', '/mantenedor-sucursales-pool/pool-vigentes/');
    }
})();