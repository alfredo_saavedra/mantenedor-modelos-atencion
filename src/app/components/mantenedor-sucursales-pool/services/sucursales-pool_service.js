(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("sucursalesPoolService", sucursalesPoolService);
        sucursalesPoolService.$inject = ['sucursalesPoolFactory'];

    function sucursalesPoolService(sucursalesPoolFactory) {
        var serv = this;
        serv.sucursalesPoolFactory = sucursalesPoolFactory;
    }

    /**
     *  @description método que obtiene listado de sucursales pool vigentes
     *  @param {object} sucursalesPoolRq
     *           {string} cui
     *           {string} nombreOficina
     *           {string} pagina
     *           {string} elementos
     *  @returns {Promise[response]}.data
     *           {array} sucursalPool
     *              {string} cui
     *              {string} nombreOficina
     *              {string} idEjecutivo
     *           {number} totalreg
     *           {object} mensajesResponse
     *              {string} codRespuesta
     *              {string} msjeRespuesta
     */
    sucursalesPoolService.prototype.obtenerSucursalesPoolVigentes = function (sucursalesPoolRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.listarSucursalesVigentes(sucursalesPoolRq);
    };

    /**
     *  @description método que obtiene listado de sucursales pool no vigentes
     *  @param {object} sucursalesPoolRq
     *           {string} cui
     *           {string} nombreOficina
     *           {string} pagina
     *           {string} elementos
     *  @returns {Promise[response]}.data
     *           {array} sucursalPool
     *              {string} cui
     *              {string} nombreOficina
     *              {string} idEjecutivo
     *              {string} fechaHora
     *           {number} totalreg
     *           {object} mensajesResponse
     *              {string} codRespuesta
     *              {string} msjeRespuesta
     */
    sucursalesPoolService.prototype.obtenerSucursalesPoolNoVigentes = function (sucursalesPoolRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.listarSucursalesNoVigentes(sucursalesPoolRq);
    };

    /**
     *  @description método que actualiza estado de un listado de sucursales pool vigente o eliminada
     *  @param {object} sucursalesPoolRq
     *           {string} idEjecutivo
     *           {string} stVigente
     *           {array} listSuc
     *              {string} cui
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesPoolService.prototype.cambiarEstadoSucursalesPool = function (cambiarEstadoSucursalesPoolRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.estadoSucursalPool(cambiarEstadoSucursalesPoolRq);
    };

    /**
     *  @description método que agrega una nueva sucursal pool
     *  @param {object} nuevaSucursalPoolRq
     *           {string} cui
     *           {string} nombreSucursal
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesPoolService.prototype.nuevaSucursalPool = function (nuevaSucursalPoolRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.agregarSucursalPool(nuevaSucursalPoolRq);
    };

    /**
     *  @description método que busca una sucursal por su cui
     *  @param {object} buscaSucursalRq
     *           {string} cui
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesPoolService.prototype.buscarSucursal = function (buscaSucursalRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.buscarSucursal(buscaSucursalRq);
    };

})();