(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("parametrosSucursalesPoolService", parametrosSucursalesPoolService);
        parametrosSucursalesPoolService.$inject = ['parametrosSucursalesPoolFactory'];

    function parametrosSucursalesPoolService(parametrosSucursalesPoolFactory) {
        var serv = this;
        serv.parametrosSucursalesPoolFactory = parametrosSucursalesPoolFactory;
    }

    /**
     *  @description método que obtiene listado de ejecutivos que han realizado eliminaciones
     *  @param {object} cargarEjecutivosRq
     *           {string} fechaInicio
     *           {string} fechaFin
     *  @returns {Promise[response]}.data
     *           {object} ejecutivosList
     *              {string} idEjecutivo
     *              {string} nombre
     *           {object} response
     *              {string} codigoRespuesta
     *              {string} mensajeRespuesta
     */
    parametrosSucursalesPoolService.prototype.cargarEjecutivos = function (cargarEjecutivosRq) {
        var serv = this;
        return serv.parametrosSucursalesPoolFactory.cargarEjecutivos(cargarEjecutivosRq);
    };

})();