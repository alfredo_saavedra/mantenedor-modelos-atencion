(function () {

    angular.module("WebBancoChile.mantenedor2020")
        .factory("sucursalesPoolFactory", sucursalesPoolFactory);
        sucursalesPoolFactory.$inject = ['$http','$filter'];

    function sucursalesPoolFactory($http,$filter) {

        var urlBaseConsulta = $filter('propertiesFilter')('${URL_API.mantenedor-sucursal-pool2020-rest}');

        return {
            listarSucursalesVigentes:listarSucursalesVigentes,
            listarSucursalesNoVigentes:listarSucursalesNoVigentes,
            estadoSucursalPool:estadoSucursalPool,
            agregarSucursalPool:agregarSucursalPool,
            buscarSucursal:buscarSucursal
        };

        function listarSucursalesVigentes(listarSucursalesRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorPool/listarSucursalesPool";
            var config = {
                method: 'POST',
                data: listarSucursalesRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function listarSucursalesNoVigentes(listarSucursalesRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorPool/listarSucNoVigentes";
            var config = {
                method: 'POST',
                data: listarSucursalesRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function estadoSucursalPool(cambiarEstadoSucursalesPoolRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorPool/estadoSucursalPool";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: cambiarEstadoSucursalesPoolRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function agregarSucursalPool(nuevaSucursalPoolRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorPool/agregarSucursalPool";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: nuevaSucursalPoolRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function buscarSucursal(cui){
            var urlConsulta = urlBaseConsulta + "/consulta/buscarSucursal/" + cui;
            var config = {
                method: 'GET',
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

    }

})();
