(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('sucursalesPoolVigentesCtrl', sucursalesPoolVigentesCtrl);

    function sucursalesPoolVigentesCtrl($scope, $modal, $injector, sucursalesPoolService,
        parametrosSucursalesPoolService, PerfilUsuarioService) {
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_POOL.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_POOL.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_POOL.TAGS");
        vm.sucursalesPoolService = sucursalesPoolService;
        vm.parametrosSucursalesPoolService = parametrosSucursalesPoolService;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.vigentesEliminados = [];
        vm.paginador = vm.VARIABLES.paginadorVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.actualizaVigentes();
        $scope.$on('actualizaVigentes', function(){
            vm.actualizaVigentes();
        });
    }

    sucursalesPoolVigentesCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaVigentes();
    };

    sucursalesPoolVigentesCtrl.prototype.actualizaVigentes = function() {
        var vm = this;
        var listadoSucursalesPoolRq = vm.getListarSucursalesPoolRq();
        vm.vigentesEliminados = [];
        vm.sucursalesPoolService.obtenerSucursalesPoolVigentes(listadoSucursalesPoolRq).then(function(rs){
            vm.servicioNoDisponible = undefined;
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.mensajesResponse.codRespuesta === vm.TAGS.listarSucursales.rs.ok.codRespuesta &&
                rs.data.mensajesResponse.msjeRespuesta === vm.TAGS.listarSucursales.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.sucursalPool;
                vm.paginador.totalItems = rs.data.totalreg;

                vm.sinResultadosMsg = (rs.data.totalreg === 0);
                vm.errorHandler = (vm.sinResultadosMsg ?
                    vm.EXCEPCIONES.sinResultadosMsg : vm.errorHandler);
            }
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    sucursalesPoolVigentesCtrl.prototype.getListarSucursalesPoolRq = function() {
        var vm = this;
        var listadoSucursalesPoolRq = {};

        if(!vm.isUndefinedOrNull(vm.filtroBusqueda)) {
            listadoSucursalesPoolRq.cui = isNaN(vm.filtroBusqueda) ? "" : vm.filtroBusqueda;
            listadoSucursalesPoolRq.nombreOficina = isNaN(vm.filtroBusqueda) ? vm.filtroBusqueda : "";
        } else {
            listadoSucursalesPoolRq.cui = "";
            listadoSucursalesPoolRq.nombreOficina = "";
        }

        listadoSucursalesPoolRq.pagina = vm.paginador.currentPage.toString();
        listadoSucursalesPoolRq.elementos = vm.paginador.itemsPerPage.selected.toString();

        return listadoSucursalesPoolRq;
    };

    sucursalesPoolVigentesCtrl.prototype.eliminarVigentes = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.sucursalesPool.vigentes.configModalConfirmaEliminacion, function(isConfirm) {
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.vigentesEliminados);
                vm.sucursalesPoolService.cambiarEstadoSucursalesPool(cambiarEstadoRq).then(function(rs){
                    if(!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.responseOk;
                    } else {
                        vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
                    }
                    vm.actualizaVigentes();
                    vm.vigentesEliminados = [];
                }, function(){
                    vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
                });
            }
        });
    };

    sucursalesPoolVigentesCtrl.prototype.eliminarVigente = function(vigente) {
        var vm = this;
        vm.limpiarExcepciones();
        vm.errorEliminaVigente = false;
        var cambiarEstadoRq = vm.getCambiarEstadoRq([vigente]);
        vm.sucursalesPoolService.cambiarEstadoSucursalesPool(cambiarEstadoRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.responseOk;
            } else {
                vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
            }
            vm.actualizaVigentes();
        }, function() {
            vm.errorEliminaVigente = true;
        });
    };

    sucursalesPoolVigentesCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};
        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.stVigente = vm.VARIABLES.sucursalesPool.eliminados.estado;
        cambiarEstadoRq.listSuc = [];
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listSuc.push({ cui: eliminado.cui });
        });

        return cambiarEstadoRq;
    };

    sucursalesPoolVigentesCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.vigentesEliminados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.vigentesEliminados.push(elemento);
        } else {
            vm.vigentesEliminados.splice(idxListadoSeleccionados, 1);
        }
    };

    sucursalesPoolVigentesCtrl.prototype.asideAgregarSucursalPool = function(size) {
        var vm = this;
        var modalInstance = vm.$modal.open({
            templateUrl: 'components/mantenedor-sucursales-pool/views/aside-agregar-sucursales-pool_tpl.html',
            animation: true,
            controller: 'agregarSucursalesPoolCtrl as vm',
            size: size,
            windowClass: 'modal-detalle',
            backdrop: 'static'
        });
        modalInstance.perfilUsuario = vm.perfilUsuario;

        modalInstance.result.then(function () {
            vm.actualizaVigentes();
        }, function () {
            vm.actualizaVigentes();
        });
    };

    sucursalesPoolVigentesCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    sucursalesPoolVigentesCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.sinResultadosMsg = false;
        vm.errorHandler = undefined;
    };

    sucursalesPoolVigentesCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroBusqueda = undefined;
    };

})();