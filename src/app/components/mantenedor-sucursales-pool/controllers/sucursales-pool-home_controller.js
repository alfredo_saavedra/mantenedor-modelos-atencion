(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('sucursalesPoolHomeCtrl', sucursalesPoolHomeCtrl);

    sucursalesPoolHomeCtrl.$inject = ['$scope', '$state', 'TABS'];

    function sucursalesPoolHomeCtrl($scope, $state, TABS){
        var vm = this;
        vm.$scope = $scope;
        $scope.$on('servicioNoDisponible', function(evt, message){
            vm.servicioNoDisponible = message;
        });
        vm.TABS = TABS.sucursalesPool;
        vm.$state = $state;
    }

    sucursalesPoolHomeCtrl.prototype.reload = function() {
        var vm = this;
        switch(vm.$state.current.name) {
            case vm.TABS.vigentes.route:
                vm.$scope.$broadcast('actualizaVigentes');
                break;
            case vm.TABS.eliminados.route:
                vm.$scope.$broadcast('actualizaEliminados');
                break;
            default:
                break;
        }
    };

})();