(function(){
    angular.module( 'WebBancoChile.mantenedor2020')
        .controller('agregarSucursalesPoolCtrl', agregarSucursalesPoolCtrl);

    function agregarSucursalesPoolCtrl($modalInstance, $injector, sucursalesPoolService, $scope){
        var vm = this;
        vm.$modalInstance = $modalInstance;
        vm.perfilUsuario = $modalInstance.perfilUsuario;
        vm.$injector = $injector;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_POOL.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_POOL.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_POOL.TAGS");
        vm.sucursalesPoolService = sucursalesPoolService;
        vm.$scope = $scope;
    }

    agregarSucursalesPoolCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.sucursalesPoolService.buscarSucursal(vm.cuiSucursalRq).then(function(rs){
            if (vm.isUndefinedOrNull(rs.data) || vm.isUndefinedOrNull(rs.data.nombreOficina)) {
                vm.errorHandler = vm.EXCEPCIONES.sinResultadosMsg;
            } else {
                vm.busquedaSucursalResponse = {
                    cui: rs.data.cui,
                    nombreOficina: rs.data.nombreOficina
                };
            }
        }, function() {
            vm.errorHandler = vm.EXCEPCIONES.servicioNoDisponible;
        });
    };

    agregarSucursalesPoolCtrl.prototype.reiniciarSucursalBuscada = function() {
        var vm = this;
        vm.busquedaSucursalResponse = undefined;
        vm.limpiarExcepciones();
    };

    agregarSucursalesPoolCtrl.prototype.crearSucursalPool = function() {
        var vm = this;
        if(vm.busquedaSucursalResponse) {
            vm.limpiarExcepciones();
            vm.sucursalesPoolService.nuevaSucursalPool(vm.busquedaSucursalResponse).then(function(rs){
                if (!vm.isUndefinedOrNull(rs.data) &&
                    rs.data.codRespuesta === vm.TAGS.agregarSucursalPool.rs.sucursalYaExiste.codRespuesta) {
                    vm.errorHandler = vm.EXCEPCIONES.warningModeloYaExiste;
                } else if (!vm.isUndefinedOrNull(rs.data) &&
                    rs.data.codRespuesta === vm.TAGS.agregarSucursalPool.rs.ok.codRespuesta) {
                    vm.creacionExitosa = vm.EXCEPCIONES.exito;
                }
            }, function(){
                vm.errorHandler = vm.EXCEPCIONES.servicioNoDisponible;
            });
        }
    };

    agregarSucursalesPoolCtrl.prototype.volverAtras = function() {
        var vm = this;
        vm.limpiarExcepciones();
    };

    agregarSucursalesPoolCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.errorHandler = undefined;
    };

    agregarSucursalesPoolCtrl.prototype.reintentar = function() {
        var vm = this;
        vm.crearSucursalPool();
    };

    agregarSucursalesPoolCtrl.prototype.cerrarAside = function() {
        var vm = this;
        vm.$modalInstance.close();
    };

    agregarSucursalesPoolCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

})();