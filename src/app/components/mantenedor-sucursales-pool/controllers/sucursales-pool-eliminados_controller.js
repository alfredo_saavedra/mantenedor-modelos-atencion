(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('sucursalesPoolEliminadosCtrl', sucursalesPoolEliminadosCtrl);

    function sucursalesPoolEliminadosCtrl($scope, $modal, $injector, sucursalesPoolService,
        parametrosSucursalesPoolService, $filter, fechaUtilService, PerfilUsuarioService){
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.fechaUtilService = fechaUtilService;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_POOL.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_POOL.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_POOL.TAGS");
        vm.sucursalesPoolService = sucursalesPoolService;
        vm.parametrosSucursalesPoolService = parametrosSucursalesPoolService;
        vm.$filter = $filter;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.eliminadosRestaurados = [];
        vm.paginador = vm.VARIABLES.paginadorNoVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.initMantenedor();
        $scope.$on('actualizaEliminados', function(){
            vm.initMantenedor();
        });
    }

    sucursalesPoolEliminadosCtrl.prototype.initMantenedor = function() {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            vm.fechaDesde = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaHasta = data;
            vm.fechaDesdeMax = data;
            vm.fechaHastaMin = data;
            vm.getFiltroEjecutivos();
            vm.actualizaEliminados();
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaEliminados();
    };

    sucursalesPoolEliminadosCtrl.prototype.reiniciaPagina = function() {
        var vm = this;
        vm.paginador.currentPage = 1;
    };

    sucursalesPoolEliminadosCtrl.prototype.getFiltroEjecutivos = function () {
        var vm = this;
        var cargarEjecutivosRq = vm.getFiltroEjecutivosRq();
        vm.parametrosSucursalesPoolService.cargarEjecutivos(cargarEjecutivosRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.response.codRespuesta === vm.TAGS.cargarEjecutivos.rs.ok.codRespuesta &&
                rs.data.response.msjeRespuesta === vm.TAGS.cargarEjecutivos.rs.ok.msjeRespuesta) {
                vm.filtrosEjecutivos = rs.data.ejecutivosList;
            }
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.getFiltroEjecutivosRq = function() {
        var vm = this;
        var filtroEjecutivos = {};

        filtroEjecutivos.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        filtroEjecutivos.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return filtroEjecutivos;
    };

    sucursalesPoolEliminadosCtrl.prototype.actualizaEliminados = function() {
        var vm = this;
        var listadoSucursalesPoolRq = vm.getEliminadosRq();
        vm.eliminadosRestaurados = [];
        vm.sucursalesPoolService.obtenerSucursalesPoolNoVigentes(listadoSucursalesPoolRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.mensajesResponse.codRespuesta === vm.TAGS.listarSucursales.rs.ok.codRespuesta &&
                rs.data.mensajesResponse.msjeRespuesta === vm.TAGS.listarSucursales.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.sucursalPool;
                vm.paginador.totalItems = rs.data.totalreg;
            }

            vm.sinResultadosMsg = (rs.data.totalreg === 0);
            vm.errorHandler = (vm.sinResultadosMsg ?
                vm.EXCEPCIONES.sinResultadosMsg : vm.errorHandler);
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.getEliminadosRq = function() {
        var vm = this;
        var listadoSucursalesPoolRq = {};

        if(!vm.isUndefinedOrNull(vm.filtroBusqueda)) {
            listadoSucursalesPoolRq.cui = isNaN(vm.filtroBusqueda) ? "" : vm.filtroBusqueda;
            listadoSucursalesPoolRq.nombreOficina = isNaN(vm.filtroBusqueda) ? vm.filtroBusqueda : "";
        } else {
            listadoSucursalesPoolRq.cui = "";
            listadoSucursalesPoolRq.nombreOficina = "";
        }

        listadoSucursalesPoolRq.idEjecutivoMod = vm.isUndefinedOrNull(
            vm.filtroEjecutivo) ? "" : vm.filtroEjecutivo.codUsuario;

        listadoSucursalesPoolRq.pagina = vm.paginador.currentPage.toString();
        listadoSucursalesPoolRq.elementos = vm.paginador.itemsPerPage.selected.toString();

        listadoSucursalesPoolRq.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        listadoSucursalesPoolRq.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return listadoSucursalesPoolRq;
    };

    sucursalesPoolEliminadosCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroBusqueda = undefined;
        vm.filtroEjecutivo = undefined;
    };

    sucursalesPoolEliminadosCtrl.prototype.filtrarPorPeriodo = function(periodo) {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            var fechaAhora = data;
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            switch(periodo){
                case vm.VARIABLES.fechas.MES.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.MES.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                case vm.VARIABLES.fechas.SEMANA.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.SEMANA.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                default:
                    break;
            }
            vm.actualizaRangoFechas();
            vm.actualizaEliminados();
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.actualizaRangoFechas = function() {
        var vm = this;
        if(vm.fechaDesde > vm.fechaHasta) {
            vm.fechaHasta = vm.fechaDesde;
        }
        vm.limpiarExcepciones();
        vm.reiniciaPagina();
        vm.getFiltroEjecutivos();
    };

    sucursalesPoolEliminadosCtrl.prototype.restaurarEliminados = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.sucursalesPool.eliminados.configModalConfirmaRestauracion, function(isConfirm){
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.eliminadosRestaurados);
                vm.sucursalesPoolService.cambiarEstadoSucursalesPool(cambiarEstadoRq).then(function(rs){
                    if (!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.sucursalYaExiste.codRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.sucursalYaExiste;
                    } else if(!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.responseOk;
                    }
                    vm.actualizaEliminados();
                    vm.eliminadosRestaurados = [];
                }, function(){
                    vm.errorHandler = vm.EXCEPCIONES.operacionFallida;
                });
            }
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.restaurarEliminado = function(eliminado) {
        var vm = this;
        vm.limpiarExcepciones();
        var cambiarEstadoRq = vm.getCambiarEstadoRq([eliminado]);
        vm.sucursalesPoolService.cambiarEstadoSucursalesPool(cambiarEstadoRq).then(function(rs){
            if (!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.sucursalYaExiste.codRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.sucursalYaExiste;
            } else if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.responseOk;
            }
            vm.actualizaEliminados();
        }, function() {
            vm.errorRestaurarEliminado = true;
        });
    };

    sucursalesPoolEliminadosCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};

        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.listSuc = [];
        cambiarEstadoRq.stVigente = vm.VARIABLES.sucursalesPool.vigentes.estado;
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listSuc.push({ cui: eliminado.cui });
        });

        return cambiarEstadoRq;
    };

    sucursalesPoolEliminadosCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.eliminadosRestaurados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.eliminadosRestaurados.push(elemento);
        } else {
            vm.eliminadosRestaurados.splice(idxListadoSeleccionados, 1);
        }
    };

    sucursalesPoolEliminadosCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    sucursalesPoolEliminadosCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.sinResultadosMsg = false;
        vm.errorHandler = undefined;
        vm.errorRestaurarEliminado = false;
    };

})();