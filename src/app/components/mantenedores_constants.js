(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .constant('TABS', {
            modelosAtencion: {
                vigentes: { heading: "Vigentes", route:"atencion-vigentes", active:true },
                eliminados: { heading: "Eliminados", route:"atencion-eliminados", active:false }
            },
            sucursalesPool: {
                vigentes: { heading: "Vigentes", route:"pool-vigentes", active:true },
                eliminados: { heading: "Eliminados", route:"pool-eliminados", active:false }
            },
            sucursalesGestor: {
                vigentes: { heading: "Vigentes", route:"gestor-vigentes", active:true },
                eliminados: { heading: "Eliminados", route:"gestor-eliminados", active:false }
            }
        });
})();