(function(){
    angular
        .module(
            'WebBancoChile.mantenedor2020', [
                'ui.router',
                'ui.bootstrap',
                'WebBancoChile.constants',
                'ui.select',
                'WebBancoChile.orionTablas',
                'WebBancoChile.ngPaginatorBack',
                'WebBancoChile.tabsComun'
        ]);
})();