(function(){
    angular.module( 'WebBancoChile.mantenedor2020')
        .controller('agregarSucursalesGestorCtrl', agregarSucursalesGestorCtrl);

    function agregarSucursalesGestorCtrl($modalInstance, $injector, sucursalesGestorService, $scope){
        var vm = this;
        vm.$modalInstance = $modalInstance;
        vm.perfilUsuario = $modalInstance.perfilUsuario;
        vm.$injector = $injector;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_GESTOR.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_GESTOR.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_GESTOR.TAGS");
        vm.sucursalesGestorService = sucursalesGestorService;
        vm.$scope = $scope;
    }

    agregarSucursalesGestorCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.sucursalesGestorService.buscarSucursal(vm.cuiSucursalRq).then(function(rs){
            if (vm.isUndefinedOrNull(rs.data) || vm.isUndefinedOrNull(rs.data.nombreOficina)) {
                vm.errorHandler = vm.EXCEPCIONES.sinResultadosMsg;
            } else {
                vm.busquedaSucursalResponse = {
                    cui: rs.data.cui,
                    nombreSucursal: rs.data.nombreOficina
                };
            }
        }, function() {
            vm.errorHandler = vm.EXCEPCIONES.servicioNoDisponible;
        });
    };

    agregarSucursalesGestorCtrl.prototype.reiniciarSucursalBuscada = function() {
        var vm = this;
        vm.busquedaSucursalResponse = undefined;
        vm.limpiarExcepciones();
    };

    agregarSucursalesGestorCtrl.prototype.crearSucursalGestor = function() {
        var vm = this;
        if(vm.busquedaSucursalResponse) {
            vm.limpiarExcepciones();
            vm.sucursalesGestorService.nuevaSucursalGestor(vm.busquedaSucursalResponse).then(function(rs){
                if (!vm.isUndefinedOrNull(rs.data) &&
                    rs.data.codRespuesta === vm.TAGS.agregarSucursalGestor.rs.sucursalYaExiste.codRespuesta) {
                    vm.errorHandler = vm.EXCEPCIONES.warningModeloYaExiste;
                } else if (!vm.isUndefinedOrNull(rs.data) &&
                    rs.data.codRespuesta === vm.TAGS.agregarSucursalGestor.rs.ok.codRespuesta) {
                    vm.creacionExitosa = vm.EXCEPCIONES.exito;
                }
            }, function(){
                vm.errorHandler = vm.EXCEPCIONES.servicioNoDisponible;
            });
        }
    };

    agregarSucursalesGestorCtrl.prototype.volverAtras = function() {
        var vm = this;
        vm.limpiarExcepciones();
    };

    agregarSucursalesGestorCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.errorHandler = undefined;
        vm.creacionExitosa = undefined;
    };

    agregarSucursalesGestorCtrl.prototype.reintentar = function() {
        var vm = this;
        vm.crearSucursalGestor();
    };

    agregarSucursalesGestorCtrl.prototype.cerrarAside = function() {
        var vm = this;
        vm.$modalInstance.close();
    };

    agregarSucursalesGestorCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

})();