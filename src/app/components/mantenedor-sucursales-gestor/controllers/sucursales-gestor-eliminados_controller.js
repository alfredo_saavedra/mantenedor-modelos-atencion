(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('sucursalesGestorEliminadosCtrl', sucursalesGestorEliminadosCtrl);

    function sucursalesGestorEliminadosCtrl($scope, $modal, $injector, sucursalesGestorService,
        parametrosSucursalesGestorService, $filter, fechaUtilService, PerfilUsuarioService){
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.fechaUtilService = fechaUtilService;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_GESTOR.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_GESTOR.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_GESTOR.TAGS");
        vm.sucursalesGestorService = sucursalesGestorService;
        vm.parametrosSucursalesGestorService = parametrosSucursalesGestorService;
        vm.$filter = $filter;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.eliminadosRestaurados = [];
        vm.paginador = vm.VARIABLES.paginadorNoVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.initMantenedor();
        $scope.$on('actualizaEliminados', function(){
            vm.initMantenedor();
        });
    }

    sucursalesGestorEliminadosCtrl.prototype.initMantenedor = function() {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            vm.fechaDesde = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaHasta = data;
            vm.fechaDesdeMax = data;
            vm.fechaHastaMin = data;
            vm.getFiltroEjecutivos();
            vm.actualizaEliminados();
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaEliminados();
    };

    sucursalesGestorEliminadosCtrl.prototype.reiniciaPagina = function() {
        var vm = this;
        vm.paginador.currentPage = 1;
    };

    sucursalesGestorEliminadosCtrl.prototype.getFiltroEjecutivos = function () {
        var vm = this;
        var cargarEjecutivosRq = vm.getFiltroEjecutivosRq();
        vm.parametrosSucursalesGestorService.cargarEjecutivos(cargarEjecutivosRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.response.codRespuesta === vm.TAGS.cargarEjecutivos.rs.ok.codRespuesta &&
                rs.data.response.msjeRespuesta === vm.TAGS.cargarEjecutivos.rs.ok.msjeRespuesta) {
                vm.filtrosEjecutivos = rs.data.ejecutivosList;
            }
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.getFiltroEjecutivosRq = function() {
        var vm = this;
        var filtroEjecutivos = {};

        filtroEjecutivos.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        filtroEjecutivos.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return filtroEjecutivos;
    };

    sucursalesGestorEliminadosCtrl.prototype.actualizaEliminados = function() {
        var vm = this;
        var listadoSucursalesGestorRq = vm.getEliminadosRq();
        vm.eliminadosRestaurados = [];
        vm.sucursalesGestorService.obtenerSucursalesGestorNoVigentes(listadoSucursalesGestorRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.mensajesResponse.codRespuesta === vm.TAGS.listarSucursales.rs.ok.codRespuesta &&
                rs.data.mensajesResponse.msjeRespuesta === vm.TAGS.listarSucursales.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.sucursalEnrolada;
                vm.paginador.totalItems = rs.data.totalreg;
            }

            vm.sinResultadosMsg = (rs.data.totalreg === 0);
            vm.errorHandler = (vm.sinResultadosMsg ?
                vm.EXCEPCIONES.sinResultadosMsg : vm.errorHandler);
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.getEliminadosRq = function() {
        var vm = this;
        var listadoSucursalesGestorRq = {};

        if(!vm.isUndefinedOrNull(vm.filtroBusqueda)) {
            listadoSucursalesGestorRq.cui = isNaN(vm.filtroBusqueda) ? "" : vm.filtroBusqueda;
            listadoSucursalesGestorRq.nombreOficina = isNaN(vm.filtroBusqueda) ? vm.filtroBusqueda : "";
        } else {
            listadoSucursalesGestorRq.cui = "";
            listadoSucursalesGestorRq.nombreOficina = "";
        }

        listadoSucursalesGestorRq.idEjecutivoMod = vm.isUndefinedOrNull(
            vm.filtroEjecutivo) ? "" : vm.filtroEjecutivo.codUsuario;

        listadoSucursalesGestorRq.pagina = vm.paginador.currentPage.toString();
        listadoSucursalesGestorRq.elementos = vm.paginador.itemsPerPage.selected.toString();

        listadoSucursalesGestorRq.fechaInicio = vm.fechaUtilService.dateToString(new Date(vm.fechaDesde));
        listadoSucursalesGestorRq.fechaFin = vm.fechaUtilService.dateToString(new Date(vm.fechaHasta));

        return listadoSucursalesGestorRq;
    };

    sucursalesGestorEliminadosCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroBusqueda = undefined;
        vm.filtroEjecutivo = undefined;
    };

    sucursalesGestorEliminadosCtrl.prototype.filtrarPorPeriodo = function(periodo) {
        var vm = this;
        vm.fechaUtilService.getFechaActual().then(function(data) {
            var fechaAhora = data;
            vm.fechaMin = vm.fechaUtilService.addDays(data, -(vm.VARIABLES.fechas.MES.ctDias));
            vm.fechaMax = data;
            switch(periodo){
                case vm.VARIABLES.fechas.MES.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.MES.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                case vm.VARIABLES.fechas.SEMANA.nombre:
                    vm.fechaDesde = vm.fechaUtilService.addDays(fechaAhora, -(vm.VARIABLES.fechas.SEMANA.ctDias));
                    vm.fechaHasta = fechaAhora;
                    break;
                default:
                    break;
            }
            vm.actualizaRangoFechas();
            vm.actualizaEliminados();
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.actualizaRangoFechas = function() {
        var vm = this;
        if(vm.fechaDesde > vm.fechaHasta) {
            vm.fechaHasta = vm.fechaDesde;
        }
        vm.limpiarExcepciones();
        vm.reiniciaPagina();
        vm.getFiltroEjecutivos();
    };

    sucursalesGestorEliminadosCtrl.prototype.restaurarEliminados = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.sucursalesGestor.eliminados.configModalConfirmaRestauracion, function(isConfirm){
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.eliminadosRestaurados);
                vm.sucursalesGestorService.cambiarEstadoSucursalesGestor(cambiarEstadoRq).then(function(rs){
                    if (!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.sucursalYaExiste.codRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.sucursalYaExiste;
                    } else if(!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.responseOk;
                    }
                    vm.actualizaEliminados();
                    vm.eliminadosRestaurados = [];
                }, function(){
                    vm.errorHandler = vm.EXCEPCIONES.operacionFallida;
                });
            }
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.restaurarEliminado = function(eleminado) {
        var vm = this;
        vm.limpiarExcepciones();
        vm.errorRestaurarEliminado = false;
        var cambiarEstadoRq = vm.getCambiarEstadoRq([eleminado]);
        vm.sucursalesGestorService.cambiarEstadoSucursalesGestor(cambiarEstadoRq).then(function(rs){
            if (!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.sucursalYaExiste.codRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.sucursalYaExiste;
            } else if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.responseOk;
            }
            vm.actualizaEliminados();
        }, function() {
            vm.errorRestaurarEliminado = true;
        });
    };

    sucursalesGestorEliminadosCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};

        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.listSuc = [];
        cambiarEstadoRq.stVigente = vm.VARIABLES.sucursalesGestor.vigentes.estado;
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listSuc.push({ cui: eliminado.cui });
        });

        return cambiarEstadoRq;
    };

    sucursalesGestorEliminadosCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.eliminadosRestaurados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.eliminadosRestaurados.push(elemento);
        } else {
            vm.eliminadosRestaurados.splice(idxListadoSeleccionados, 1);
        }
    };

    sucursalesGestorEliminadosCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    sucursalesGestorEliminadosCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.sinResultadosMsg = false;
        vm.errorHandler = undefined;
    };

})();