(function() {
    angular.module( 'WebBancoChile.mantenedor2020')
           .controller('sucursalesGestorVigentesCtrl', sucursalesGestorVigentesCtrl);

    function sucursalesGestorVigentesCtrl($scope, $modal, $injector, sucursalesGestorService,
        parametrosSucursalesGestorService, PerfilUsuarioService) {
        var vm = this;
        vm.$scope = $scope;
        vm.$modal = $modal;
        vm.EXCEPCIONES = $injector.get("SUCURSALES_GESTOR.EXCEPCIONES");
        vm.VARIABLES = $injector.get("SUCURSALES_GESTOR.VARIABLES");
        vm.TAGS = $injector.get("SUCURSALES_GESTOR.TAGS");
        vm.sucursalesGestorService = sucursalesGestorService;
        vm.parametrosSucursalesGestorService = parametrosSucursalesGestorService;
        PerfilUsuarioService.getUserProfile().then(function (data) {
            vm.perfilUsuario = data;
        });

        vm.vigentesEliminados = [];
        vm.paginador = vm.VARIABLES.paginadorVigentes;
        vm.paginador.itemsPerPage.selected = vm.paginador.pageSize;

        vm.actualizaVigentes();
        $scope.$on('actualizaVigentes', function(){
            vm.actualizaVigentes();
        });
    }

    sucursalesGestorVigentesCtrl.prototype.buscar = function() {
        var vm = this;
        vm.limpiarExcepciones();
        vm.actualizaVigentes();
    };

    sucursalesGestorVigentesCtrl.prototype.actualizaVigentes = function() {
        var vm = this;
        var listadoSucursalesGestorRq = vm.getListarSucursalesGestorRq();
        vm.vigentesEliminados = [];
        vm.sucursalesGestorService.obtenerSucursalesGestorVigentes(listadoSucursalesGestorRq).then(function(rs){
            vm.servicioNoDisponible = undefined;
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.mensajesResponse.codRespuesta === vm.TAGS.listarSucursales.rs.ok.codRespuesta &&
                rs.data.mensajesResponse.msjeRespuesta === vm.TAGS.listarSucursales.rs.ok.msjeRespuesta) {
                vm.paginador.pagedItems = rs.data.sucursalEnrolada;
                vm.paginador.totalItems = rs.data.totalreg;

                vm.sinResultadosMsg = (rs.data.totalreg === 0);
                vm.errorHandler = (vm.sinResultadosMsg ?
                    vm.EXCEPCIONES.sinResultadosMsg : vm.errorHandler);
            }
        }, function() {
            vm.$scope.$emit('servicioNoDisponible', vm.EXCEPCIONES.servicioNoDisponible);
        });
    };

    sucursalesGestorVigentesCtrl.prototype.getListarSucursalesGestorRq = function() {
        var vm = this;
        var listadoSucursalesGestorRq = {};

        if(!vm.isUndefinedOrNull(vm.filtroBusqueda)) {
            listadoSucursalesGestorRq.cui = isNaN(vm.filtroBusqueda) ? "" : vm.filtroBusqueda;
            listadoSucursalesGestorRq.nombreOficina = isNaN(vm.filtroBusqueda) ? vm.filtroBusqueda : "";
        } else {
            listadoSucursalesGestorRq.cui = "";
            listadoSucursalesGestorRq.nombreOficina = "";
        }

        listadoSucursalesGestorRq.pagina = vm.paginador.currentPage.toString();
        listadoSucursalesGestorRq.elementos = vm.paginador.itemsPerPage.selected.toString();

        return listadoSucursalesGestorRq;
    };

    sucursalesGestorVigentesCtrl.prototype.eliminarVigentes = function() {
        var vm = this;
        vm.limpiarExcepciones();
        swal(vm.VARIABLES.sucursalesGestor.vigentes.configModalConfirmaEliminacion, function(isConfirm) {
            if (isConfirm) {
                var cambiarEstadoRq = vm.getCambiarEstadoRq(vm.vigentesEliminados);
                vm.sucursalesGestorService.cambiarEstadoSucursalesGestor(cambiarEstadoRq).then(function(rs){
                    if(!vm.isUndefinedOrNull(rs.data) &&
                        rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                        rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                        vm.errorHandler = vm.EXCEPCIONES.responseOk;
                    } else {
                        vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
                    }
                    vm.actualizaVigentes();
                    vm.vigentesEliminados = [];
                }, function(){
                    vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
                });
            }
        });
    };

    sucursalesGestorVigentesCtrl.prototype.eliminarVigente = function(vigente) {
        var vm = this;
        vm.limpiarExcepciones();
        vm.errorEliminaVigente = false;
        var cambiarEstadoRq = vm.getCambiarEstadoRq([vigente]);
        vm.sucursalesGestorService.cambiarEstadoSucursalesGestor(cambiarEstadoRq).then(function(rs){
            if(!vm.isUndefinedOrNull(rs.data) &&
                rs.data.codRespuesta === vm.TAGS.estadoSucursal.rs.ok.codRespuesta &&
                rs.data.msjeRespuesta === vm.TAGS.estadoSucursal.rs.ok.msjeRespuesta) {
                vm.errorHandler = vm.EXCEPCIONES.responseOk;
            } else {
                vm.errorHandler = vm.EXCEPCIONES.errorEliminarVigentes;
            }
            vm.actualizaVigentes();
        }, function() {
            vm.errorEliminaVigente = true;
        });
    };

    sucursalesGestorVigentesCtrl.prototype.getCambiarEstadoRq = function(listadoEliminados) {
        var vm = this;
        var cambiarEstadoRq = {};
        cambiarEstadoRq.idEjecutivo = vm.perfilUsuario.usuario;
        cambiarEstadoRq.stVigente = vm.VARIABLES.sucursalesGestor.eliminados.estado;
        cambiarEstadoRq.listSuc = [];
        listadoEliminados.forEach(function(eliminado){
            cambiarEstadoRq.listSuc.push({ cui: eliminado.cui });
        });

        return cambiarEstadoRq;
    };

    sucursalesGestorVigentesCtrl.prototype.actualizaListaSeleccionados = function(elemento) {
        var vm = this;
        var idxListadoSeleccionados = vm.vigentesEliminados.indexOf(elemento);
        if (idxListadoSeleccionados === -1){
            vm.vigentesEliminados.push(elemento);
        } else {
            vm.vigentesEliminados.splice(idxListadoSeleccionados, 1);
        }
    };

    sucursalesGestorVigentesCtrl.prototype.asideAgregarSucursalGestor = function(size) {
        var vm = this;
        var modalInstance = vm.$modal.open({
            templateUrl: 'components/mantenedor-sucursales-gestor/views/aside-agregar-sucursales-gestor_tpl.html',
            animation: true,
            controller: 'agregarSucursalesGestorCtrl as vm',
            size: size,
            windowClass: 'modal-detalle',
            backdrop: 'static'
        });
        modalInstance.perfilUsuario = vm.perfilUsuario;

        modalInstance.result.then(function () {
            vm.actualizaVigentes();
        }, function () {
            vm.actualizaVigentes();
        });
    };

    sucursalesGestorVigentesCtrl.prototype.isUndefinedOrNull = function(a) {
        return (angular.isUndefined(a) || a === null);
    };

    sucursalesGestorVigentesCtrl.prototype.limpiarExcepciones = function() {
        var vm = this;
        vm.sinResultadosMsg = false;
        vm.errorHandler = undefined;
    };

    sucursalesGestorVigentesCtrl.prototype.limpiarFiltros = function () {
        var vm = this;
        vm.filtroBusqueda = undefined;
    };

})();