(function() {
    angular
        .module('WebBancoChile.mantenedor2020')
        .config(mantenedorGestor2020Config);

    function mantenedorGestor2020Config($stateProvider, $urlRouterProvider, TABS) {
        $stateProvider.state('mantenedor-sucursales-gestor', {
            parent: 'root',
            url: '/mantenedor-sucursales-gestor',
            views: {
                "main@": {
                    controller: 'sucursalesGestorHomeCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-gestor/views/sucursales-gestor-home_tpl.html'
                }
            }, data: { pageTitle: 'Mantenedor Sucursales Gestor 2020' }
        })
        .state('gestor-vigentes', {
            parent: 'mantenedor-sucursales-gestor',
            url: '/gestor-vigentes/',
            views: {
                "contenido_tab_view": {
                    controller: 'sucursalesGestorVigentesCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-gestor/views/sucursales-gestor-vigentes_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.sucursalesGestor.vigentes, TABS.sucursalesGestor);
                }
            }
        })
        .state('gestor-eliminados', {
            parent: 'mantenedor-sucursales-gestor',
            url: '/gestor-eliminados/',
            views: {
                "contenido_tab_view": {
                    controller: 'sucursalesGestorEliminadosCtrl as vm',
                    templateUrl: 'components/mantenedor-sucursales-gestor/views/sucursales-gestor-eliminadas_tpl.html'
                }
            },
            resolve: {
                tabActive: function(tabsComunService){
                    return tabsComunService.setActiveTab(TABS.sucursalesGestor.eliminados, TABS.sucursalesGestor);
                }
            }
        });

        $urlRouterProvider.when('/mantenedor-sucursales-gestor', '/mantenedor-sucursales-gestor/gestor-vigentes/');
    }
})();