(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("sucursalesGestorService", sucursalesGestorService);
        sucursalesGestorService.$inject = ['sucursalesGestorFactory', 'sucursalesPoolFactory'];

    function sucursalesGestorService(sucursalesGestorFactory, sucursalesPoolFactory) {
        var serv = this;
        serv.sucursalesGestorFactory = sucursalesGestorFactory;
        serv.sucursalesPoolFactory = sucursalesPoolFactory;
    }

    /**
     *  @description método que obtiene listado de sucursales con gestor vigentes
     *  @param {object} sucursalesGestorRq
     *           {string} cui
     *           {string} nombreOficina
     *           {string} pagina
     *           {string} elementos
     *  @returns {Promise[response]}.data
     *           {array} sucursalGestor
     *              {string} cui
     *              {string} nombreOficina
     *              {string} idEjecutivo
     *           {number} totalreg
     *           {object} mensajesResponse
     *              {string} codRespuesta
     *              {string} msjeRespuesta
     */
    sucursalesGestorService.prototype.obtenerSucursalesGestorVigentes = function (sucursalesGestorRq) {
        var serv = this;
        return serv.sucursalesGestorFactory.listarSucursalesVigentes(sucursalesGestorRq);
    };

    /**
     *  @description método que obtiene listado de sucursales con gestor no vigentes
     *  @param {object} sucursalesGestorRq
     *           {string} cui
     *           {string} nombreOficina
     *           {string} pagina
     *           {string} elementos
     *  @returns {Promise[response]}.data
     *           {array} sucursalGestor
     *              {string} cui
     *              {string} nombreOficina
     *              {string} idEjecutivo
     *              {string} fechaHora
     *           {number} totalreg
     *           {object} mensajesResponse
     *              {string} codRespuesta
     *              {string} msjeRespuesta
     */
    sucursalesGestorService.prototype.obtenerSucursalesGestorNoVigentes = function (sucursalesGestorRq) {
        var serv = this;
        return serv.sucursalesGestorFactory.listarSucursalesNoVigentes(sucursalesGestorRq);
    };

    /**
     *  @description método que actualiza estado de un listado de sucursales con gestor vigente o eliminada
     *  @param {object} sucursalesGestorRq
     *           {string} idEjecutivo
     *           {string} stVigente
     *           {array} listSuc
     *              {string} cui
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesGestorService.prototype.cambiarEstadoSucursalesGestor = function (cambiarEstadoSucursalesGestorRq) {
        var serv = this;
        return serv.sucursalesGestorFactory.estadoSucursalGestor(cambiarEstadoSucursalesGestorRq);
    };

    /**
     *  @description método que agrega una nueva sucursal con gestor
     *  @param {object} nuevaSucursalGestorRq
     *           {string} cui
     *           {string} nombreSucursal
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesGestorService.prototype.nuevaSucursalGestor = function (nuevaSucursalGestorRq) {
        var serv = this;
        return serv.sucursalesGestorFactory.agregarSucursalGestor(nuevaSucursalGestorRq);
    };

    /**
     *  @description método que busca una sucursal por su cui
     *  @param {object} buscaSucursalRq
     *           {string} cui
     *  @returns {Promise[response]}.data
     *           {string} codRespuesta
     *           {string} msjeRespuesta
     */
    sucursalesGestorService.prototype.buscarSucursal = function (buscaSucursalRq) {
        var serv = this;
        return serv.sucursalesPoolFactory.buscarSucursal(buscaSucursalRq);
    };

})();