(function () {
    angular.module("WebBancoChile.mantenedor2020")
        .service("parametrosSucursalesGestorService", parametrosSucursalesGestorService);
        parametrosSucursalesGestorService.$inject = ['parametrosSucursalesGestorFactory'];

    function parametrosSucursalesGestorService(parametrosSucursalesGestorFactory) {
        var serv = this;
        serv.parametrosSucursalesGestorFactory = parametrosSucursalesGestorFactory;
    }

    /**
     *  @description método que obtiene listado de ejecutivos que han realizado eliminaciones de sucursales con gestor
     *  @param {object} cargarEjecutivosRq
     *           {string} fechaInicio
     *           {string} fechaFin
     *  @returns {Promise[response]}.data
     *           {object} ejecutivosList
     *              {string} idEjecutivo
     *              {string} nombre
     *           {object} response
     *              {string} codigoRespuesta
     *              {string} mensajeRespuesta
     */
    parametrosSucursalesGestorService.prototype.cargarEjecutivos = function (cargarEjecutivosRq) {
        var serv = this;
        return serv.parametrosSucursalesGestorFactory.cargarEjecutivos(cargarEjecutivosRq);
    };

})();