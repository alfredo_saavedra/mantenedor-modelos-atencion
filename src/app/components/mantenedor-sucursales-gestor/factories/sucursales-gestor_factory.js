(function () {

    angular.module("WebBancoChile.mantenedor2020")
        .factory("sucursalesGestorFactory", sucursalesGestorFactory);
        sucursalesGestorFactory.$inject = ['$http','$filter'];

    function sucursalesGestorFactory($http,$filter) {

        var urlBaseConsulta = $filter('propertiesFilter')('${URL_API.mantenedor-sucursal-gestor2020-rest}');

        return {
            listarSucursalesVigentes:listarSucursalesVigentes,
            listarSucursalesNoVigentes:listarSucursalesNoVigentes,
            estadoSucursalGestor:estadoSucursalGestor,
            agregarSucursalGestor:agregarSucursalGestor
        };

        function listarSucursalesVigentes(listarSucursalesRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorGestor/listarSucursalesEnroladas";
            var config = {
                method: 'POST',
                data: listarSucursalesRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function listarSucursalesNoVigentes(listarSucursalesRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorGestor/listarSucNoVigentes";
            var config = {
                method: 'POST',
                data: listarSucursalesRq,
                url: urlConsulta,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function estadoSucursalGestor(cambiarEstadoSucursalesGestorRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorGestor/estadoSucursalGestor";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: cambiarEstadoSucursalesGestorRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

        function agregarSucursalGestor(nuevaSucursalGestorRq){
            var urlConsulta = urlBaseConsulta + "/mantenedorGestor/agregarSucursalGestor";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: nuevaSucursalGestorRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }
    }

})();
