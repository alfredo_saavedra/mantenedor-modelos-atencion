(function () {

    angular.module("WebBancoChile.mantenedor2020")
        .factory("parametrosSucursalesGestorFactory", parametrosSucursalesGestorFactory);
        parametrosSucursalesGestorFactory.$inject = ['$http','$filter'];

    function parametrosSucursalesGestorFactory($http,$filter) {

        var urlBaseConsulta = $filter('propertiesFilter')('${URL_API.mantenedor-sucursal-gestor2020-rest}');

        return {
            cargarEjecutivos:cargarEjecutivos
        };

        function cargarEjecutivos(cargarEjecutivosRq){
            var urlConsulta =  urlBaseConsulta +  "/consulta/cargarEjecutivos";
            var config = {
                method: 'POST',
                url: urlConsulta,
                data: cargarEjecutivosRq,
                headers: {'Content-Type': 'application/json'}
            };
            return $http(config);
        }

    }

})();
