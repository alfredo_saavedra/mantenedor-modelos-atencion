(function () {
        angular
            .module('WebBancoChile', [
                    'ui.router',
                    'ui.router.state.events',
                    'ui.bootstrap',
                    'ui.utils',
                    'ui.select',
                    'ngAnimate',
                    'templates-app',
                    'templates-taurus',
                    'templates-common',
                    'templates-orion',
                    'angular-loading-bar',
                    'WebBancoChile.ngDate',
                    'WebBancoChile.filters',
                    'WebBancoChile.constants',
                    'WebBancoChile.jwt',
                    'WebBancoChile.errorHandler',
                    'WebBancoChile.perfilusuario',
                    'WebBancoChile.properties',
                    'WebBancoChile.showIfAuthorized',
                    'WebBancoChile.mantenedor2020',
                    'WebBancoChile.ngPaginator',
                    'WebBancoChile.onlyNumberDirective'
            ]);
})();
