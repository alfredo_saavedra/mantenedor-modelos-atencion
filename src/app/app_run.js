/**
 * Created by c.zumelzu.scheel on 12/28/2016.
 */
(function() {
    angular
        .module('WebBancoChile')
        .run(appRun);
    appRun.$inject = ['$rootScope'];
    function appRun($rootScope) {
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            if (error!=null && error.code !=null) {
                switch (error.code) {
                    case 403:
                        window.location.href = '/plataforma-web/login/index.html#/403';
                        break;
                    case 401:
                        window.location.href = '/login/logout/platerror/401';
                        break;
                    case 500:
                        window.location.href = '/login/logout/platerror/500';
                        break;
                    default:
                        window.location.href = '/login/logout/platcom';
                        break;
                }
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (angular.isDefined(toState.data.pageTitle)) {
                $rootScope.pageTitle = toState.data.pageTitle + ' | Banco de Chile';
            }
        });
    }
})();