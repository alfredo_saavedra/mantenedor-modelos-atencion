/**
 * Created by c.zumelzu.scheel on 12/28/2016.
 */
(function() {
    angular
        .module('WebBancoChile')
        .factory('TokenInterceptor', tokenInterceptor);
    tokenInterceptor.$inject = ['$q'];
    function tokenInterceptor($q) {
        var service = {
            request: request,
            responseError: responseError
        };
        return service;
        function request(httpConfig) {
            var token = localStorage.token;
            if (token) {
                httpConfig.headers['X-AUTH-TOKEN'] = token;
            }
            return httpConfig;
        }
        function responseError(response) {
            return $q.reject(response);
        }
    }
})();