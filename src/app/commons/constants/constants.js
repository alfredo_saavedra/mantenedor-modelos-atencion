(function() {
    var MENU = {};
    var PERMISSIONS  = {};
    angular
        .module('WebBancoChile.constants',[])
        .constant('rutBancoChile','12112693-1')
        .constant('dummy_datos_usuario','assets/dummy-files/datos-usuario-dummy.json')
        .constant('dummy_ultimoAcceso','assets/dummy-files/datos-ultimoAcceso-dummy.json')
        .constant('dummy_productos','assets/dummy-files/datos-producto-dummy.json')
        .constant('MENU_ARRAY',MENU)
        .constant('PERMISSIONS', PERMISSIONS);
})();