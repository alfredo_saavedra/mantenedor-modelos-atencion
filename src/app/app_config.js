(function () {
    angular.module('WebBancoChile').config(myAppConfig);

    myAppConfig.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        '$animateProvider',
        '$logProvider',
        'cfpLoadingBarProvider',
        '$httpProvider'];
    function myAppConfig(
        $stateProvider,
        $urlRouterProvider,
        $animateProvider,
        $logProvider,
        cfpLoadingBarProvider,
        $httpProvider
    ){

        $logProvider.debugEnabled(true);
        cfpLoadingBarProvider.includeSpinner = true;
        $animateProvider.classNameFilter(/animate-/);
        $urlRouterProvider.otherwise('/mantenedor-modelos-atencion/atencion-vigentes/');
        $httpProvider.interceptors.push('TokenInterceptor');

        $stateProvider.state('root', {
            abstract: true,
            resolve:{
                userProfile: cargarPropertiesYEjecutar(getUserProfile)
            },
            views: {}
        });

        function getUserProfile($injector) {
            var $q = $injector.get("$q");
            var $window = $injector.get("$window");
            var PerfilUsuarioService = $injector.get("PerfilUsuarioService");
            var defer = $q.defer();
            PerfilUsuarioService.getUserProfile().then(function (data) {
                $window.localStorage.perfilusuario = JSON.stringify(data);
                defer.resolve(data);
            }, function (err) {
                defer.reject(err);
            });
            return defer.promise;
        }

        function cargarPropertiesYEjecutar(funcion) {
            return ['$q', '$injector', 'propertiesService', function ($q, $injector, propertiesService) {
                var defer = $q.defer();
                propertiesService.loadProperties().then(function () {
                    funcion($injector).then(function (data) {
                        defer.resolve(data);
                    }, function (err) {
                        defer.reject(err);
                    });
                }, function (errRemoto) {
                    propertiesService.loadLocalProperties('assets/properties/properties.json').then(function () {
                        funcion($injector).then(function (data) {
                            defer.resolve(data);
                        }, function (err) {
                            defer.reject(err);
                        });
                    }, function (errLocal) {
                        defer.reject(errRemoto, errLocal);
                    });
                });
                return defer.promise;
            }];
        }

    }
})();