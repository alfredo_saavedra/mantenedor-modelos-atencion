/**
 * Node Configuration
 *
 * @author Banco de Chile
 *
 */
var info = require('./package.json');
var open = require('open');
var colors = require('colors');
var httpProxy = require('http-proxy');
var connect = require('connect');
var http = require('http');

var options = {
    'static': 'http://localhost:3000',
    
};

var CONTEXTOS_DISPONIBLES = {
    "miplataforma": 'http://otd-priv:20015',
    "miplataforma-core": 'http://otd-priv:10015',
    "miplataforma-incubadora": 'http://otd-priv:30015',
    "miplataforma-satellite": 'http://otd-priv:40015'
};
/*
var CONTEXTOS_DISPONIBLES = {
    "miplataforma": 'http://200.14.166.72:7013',
    "miplataforma-core": 'http://200.14.166.72:10010',
    "miplataforma-incubadora": 'http://200.14.166.72:30010',
    "miplataforma-satellite": 'http://200.14.166.72:20010'
 };
*/
/*
var CONTEXTOS_DISPONIBLES_QA = {
    "miplataforma": 'http://200.14.169.120:20015',
    "miplataforma-core": 'http://200.14.169.120:21016',
    "miplataforma-incubadora": 'http://200.14.169.120:21019',
    "miplataforma-satellite": 'http://200.14.169.120:21017'
};
*/
var proxy = httpProxy.createProxyServer({});

var proxyApp = connect().use(function(req,res) {
    console.log(('request to node server: ' + req.url).green);

    //req.headers.OAM_REMOTE_KEY = "83116005";
    //req.headers.OAM_REMOTE_USER = "cpontillo";
    
    //req.headers.OAM_REMOTE_KEY = "153837635";
    //req.headers.OAM_REMOTE_USER = "carias";

    //req.headers.OAM_REMOTE_KEY = "96646992";
    //req.headers.OAM_REMOTE_USER = "malmendras";

     req.headers.OAM_REMOTE_KEY = "85435272";
     req.headers.OAM_REMOTE_USER = "gbanda";
     
     //req.headers.OAM_REMOTE_KEY = "76201994";
     //req.headers.OAM_REMOTE_USER = "IMIRANDA";

    var target = options.static;

    var contexto = getContexto(req.url);
    var server = CONTEXTOS_DISPONIBLES[contexto];
    if(server) {
        target = server;
        console.log(('request to rest api: ' + target + req.url).blue);
    }
    proxy.web(req, res, {
        target: target
    });
});

http.createServer(proxyApp).listen(8000);
console.info('Running HTTP Proxy');

var app = connect()
    .use(connect.static('www/'))
    .use(connect.directory('www/'))
    .use(connect.cookieParser())
    .use(connect.session({ secret: 'my secret here' }));

http.createServer(app).listen(3000, openBrowser);


function openBrowser(){
    console.log('Open browser'.green);
    console.log('Running Web Server'.green);
}

function getContexto(urlConsulta) {
    var indexFin = urlConsulta.substr(1).indexOf("/");
    return urlConsulta.substr(1,indexFin);
}